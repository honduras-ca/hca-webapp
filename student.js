
/*
	student.js: Functionality for creating, editing, deleting, and getting students and their associated attendance data.
*/

var con = require('./database.js').connection;
var moment = require('moment');

module.exports = {

	/*	Add a new student profile to the DB, given that student’s info. If all required fields are not given, function will callback on error.
		Profile must AT LEAST have name and isAdult
		Callback on  error and UID of created student */
	createStudent: function(profile, callback) {
		// ensure profile is defined
		if (profile != null) {
			// ensure profile has name and isAdult (required fields)
			if (profile.name != null && profile.name != '' && (profile.isAdult == '0' || profile.isAdult == '1')) {
				// create new student with values from profile input; select the inserted record's UID
				con.query('INSERT INTO students (name, isAdult, parent1, parent2, address, phone, specialCircumstances, uniformSize) VALUES (?,?,?,?,?,?,?,?); SELECT LAST_INSERT_ID() AS uid;', [profile.name, profile.isAdult, profile.parent1, profile.parent2, profile.address, profile.phone, profile.specialCircumstances, profile.uniformSize], function(err, rows) {
					if (!err && rows !== undefined && rows.length > 1 && rows[1].length > 0) {
						// callback on inserted student's UID
						callback(err, rows[1][0].uid);
					} else {
						// errored
						callback(err || "Unable to add new student to database");
					}
				});
			} else {
				// needs to have both name and isAdult
				callback('Must have both name and isAdult');
			}
		} else {
			// needs a profile
			callback('No profile given');
		}
	},

	/*	Updates an existing student profile by posting a profile to override it with. 
		Function will run an UPDATE query that replaces all fields existing under the given student UID with the new fields.
		Callback on error */
	editStudent: function(updatedProfile, callback) { //I assume here that updatedProfile includes the previous profile's uid
		// ensure UID is given, as well as nonempty name
		if (updatedProfile.uid && updatedProfile.name && updatedProfile.name != '') {
			// do the update
			con.query('UPDATE students SET name = ?, isAdult = ?, parent1 = ?, parent2 = ?, address = ?, phone = ?, specialCircumstances = ?, uniformSize = ? WHERE uid = ?;', [updatedProfile.name, updatedProfile.isAdult, updatedProfile.parent1, updatedProfile.parent2, updatedProfile.address, updatedProfile.phone, updatedProfile.specialCircumstances, updatedProfile.uniformSize, updatedProfile.uid], function(err) {
				// give error
				callback(err);
			});
		} else {
			// must have uid and name
			callback('Updated profile must have at least a UID and name.');
		}
	},

	/*	Delete a student profile from the DB. Individual records associated with this student delete as well (ON DELETE CASCADE).
		Callback on error. */
	deleteStudent: function(uid, callback) {
		// ensure given UID exists
		if (uid != null) {
			// delete student, associated records will be automatically removed
			con.query('DELETE FROM students WHERE uid = ?;', [uid], function(err) {
				// report error
				callback(err);
			});
		} else {
			// failed
			callback('No student uid given');
		}
	},

	/*	Gets all the info associated with a given student.
		Callback on error and full student profile. */
	getStudentProfile: function(studentUID, callback) {
		// ensure UID exists
		if (studentUID != null) {
			// select row from students table
			con.query('SELECT * FROM students WHERE uid = ?;', [studentUID], function(err, rows) {
				if (!err && rows !== undefined && rows.length > 0) {
					// callback on full row of student with given uid
					callback(err, rows[0]);
				} else {
					callback(err || "No student found.");
				}
			});
		} else {
			// need uid
			callback('No student uid given.');
		}
	},

	/*	Gets all records associated with a given student within an inclusive date range.
		Callback on error and records array */
	getRecordsByStudent: function(startDate, endDate, studentUID, callback) {
		// ensure date given exists and is valid moment object
		if (startDate && startDate.isValid() && endDate && endDate.isValid()) {
			// ensure valid date range
			if (startDate.isBefore(endDate) || startDate.isSame(endDate)) {
				// get dates as formatted strings for query (and move bounds so range is inclusive)
				var startString = startDate.format('YYYY-MM-DD');
				var endString = endDate.add(1, 'day').format('YYYY-MM-DD');

				// ensure student UID exists
				if (studentUID != null) {
					// run query to get records
					con.query('SELECT i.*, a.whenRecorded, r.name AS classroom, t.name AS classType FROM individualRecords i JOIN attendanceReports a ON i.reportUID = a.uid JOIN classrooms r ON a.classroomUID = r.uid JOIN classTypes t ON a.classTypeUID = t.uid WHERE i.studentUID = ? AND (a.whenRecorded BETWEEN ? AND ?) ORDER BY a.whenRecorded DESC;', [studentUID, startString, endString], function(err, rows) {
						if (!err && rows !== undefined) {

							// reformat date of each record
							for (var i = 0; i < rows.length; i++) {
								rows[i].whenRecorded = moment(rows[i].whenRecorded).format('MMMM Do, YYYY')
							}

							// callback on reports found, if any
							callback(err, rows);
						} else {
							callback(err || "No records found.");
						}
					});
				} else {
					// callback on null UID error
					callback('No student uid given');
				}
			} else {
				// catch case where end is before start
				callback("Given end date is before start date.");
			}
		} else {
			// callback on date error
			callback("Invalid dates given.");
		}
	},

	/*	Gets the classroom / type combination this student most frequently attends (e.g. PEP-2 AM). Works same for adults or kids
		Callback on error, usual classroom, and usual class type */
	getUsualClassData: function(studentUID, callback) {
		// ensure student UID exists
		if (studentUID != null) {
			// run query to count frequencies of each room/type pair for this student
			con.query('SELECT COUNT(i.uid) AS frequency, a.classroomUID, a.classTypeUID, r.name AS classroom, t.name AS classType FROM individualRecords i JOIN attendanceReports a ON i.reportUID = a.uid JOIN classrooms r ON a.classroomUID = r.uid JOIN classTypes t ON a.classTypeUID = t.uid WHERE i.studentUID = ? GROUP BY a.classroomUID, a.classTypeUID;', [studentUID], function(err, rows) {
				if (!err && rows !== undefined) {
					// count total number of individual records retrieved
					var totalRecords = 0;

					// for each room/type pair
					for (var i = 0; i < rows.length; i++) {
						// add frequency of this pair to total records
						totalRecords += rows[i].frequency;
					}

					var max;

					// for each room/type pair, again
					for (var i = 0; i < rows.length; i++) {
						// calculate percentage frequency of attendance at this pair
						rows[i].frequency /= totalRecords;

						if (!max || rows[i].frequency > max.frequency) {
							max = rows[i];
						}
					}

					if (max) {
						// callback on classroom name, class type name, percent frequency
						callback(err, max.classroom, max.classType, max.frequency * 100);
					} else {
						// callback on null values (unable to find usual class)
						callback(err);
					}
				} else {
					// callback on error
					callback(err || "No class records found.");
				}
			});
		} else {
			// error on no UID given
			callback('No student uid given');
		}
	},

	/*	Gets all disciplinary points recorded for a given student since a given date cutoff. (e.g. since 2 months ago)
		Callback on error and records array containing date, number of disciplinary points, and UID of corresponding report */
	getDisciplinaryDataForStudent: function(dateCutoff, studentUID, callback) {
		// ensure date cutoff provided exists and is valid moment object
		if (dateCutoff && dateCutoff.isValid()) {
			// get date as formatted string for query
			var dateString = dateCutoff.format('YYYY-MM-DD');

			// ensure student UID exists
			if (studentUID != null) {
				// run query to get disciplinary data
				con.query('SELECT i.disciplinePoints AS y, DATE_FORMAT(a.whenRecorded, "%Y-%m-%d") AS x, i.reportUID FROM individualRecords i JOIN attendanceReports a ON i.reportUID = a.uid WHERE i.studentUID = ? AND a.whenRecorded > ? ORDER BY a.whenRecorded ASC;', [studentUID, dateString], function(err, rows) {
					if (!err && rows !== undefined) {
						// give discipline points, time, from given date for student
						callback(err, rows);
					} else {
						// callback on error 
						callback(err || "No disciplinary records found.");
					}
				});
			} else {
				//gib error
				callback('No student uid given');
			}
		} else {
			// callback on date cutoff error
			callback("Invalid date cutoff given.");
		}
	},

	/*	Gets top N results of student profiles whose names most closely match the given string.
		If query empty, will select all students in no particular order.
		Callback on error and students array with matched profiles */
	searchStudents: function(name, numResults, callback) {
		// add wildcard to search query to search partial
		var query = name == "" ? name : name + "*";

		// select students, scoring them against text query
		con.query('SELECT uid, name, isAdult, parent1, parent2, MATCH (name) AGAINST (? IN BOOLEAN MODE) AS score FROM students WHERE MATCH (name) AGAINST (? IN BOOLEAN MODE) OR ? = "" ORDER BY score DESC;', [query, query, query], function(err, rows) {
			if (!err && rows !== undefined) {
				// get first names of parents of each student for displaying in the search results
				for (var i = 0; i < rows.length; i++) {
					rows[i].parentString = module.exports.getParentFirstNames(rows[i]);
				}

				// callback on top N results
				callback(err, rows.slice(0, numResults));
			} else {
				// callback on error
				callback(err || "No students found.");
			}
		});
	},

	// Get the UID, Name, and pretty formatted parent's name of each student
	getAllStudents(callback) {
		// select students, scoring them against text query
		con.query('SELECT uid, name, parent1, parent2, isAdult FROM students;', function(err, rows) {
			if (!err && rows !== undefined) {
				// get first names of parents of each student for displaying in the search results
				for (var i = 0; i < rows.length; i++) {
					rows[i].parentString = module.exports.getParentFirstNames(rows[i]);
				}

				// callback on top N results
				callback(err, rows);
			} else {
				// callback on error
				callback(err || "No students found.");
			}
		});
	},

	/*	Gets the UID and name of every existing student in the database, for searching.
	Callback on error and students array with uid, name */
	getSearchableStudents(callback) {
		// select uid and name of every student
		con.query('SELECT uid, name FROM students;', function(err, rows) {
			if (!err && rows !== undefined && rows.length > 0) {
				callback(err, rows);
			} else {
				callback(err || "No students found.");
			}
		});
	},

	/*	Gets a string summarizing the parent info of a given student profile.
		If both parents exist, use: "<parent 1> & <parent 2>"
		If one is null, use: "<non-null parent>"
		If both are null, use: null
		Returns parent string */
	getParentString: function(profile) {
		// if both parent names exist
		if (profile.parent1 && profile.parent2) {
			return profile.parent1 + " & " + profile.parent2;

		// if just one parent name exists
		} else if (profile.parent1 || profile.parent2) {
			return profile.parent1 || profile.parent2;

		// if neither exists, use null (mustache will not render)
		} else {
			return null;
		}
	},

	/*	Gets a string with parents first names.
	If both parents exist, use: "<parent 1> & <parent 2>"
	If one is null, use: "<non-null parent>"
	If both are null, use: null
	Returns parent string */
	getParentFirstNames: function(profile) {
		// if both parent names exist
		if (profile.parent1 && profile.parent2) {
			// return & of both names
			return getFirstName(profile.parent1) + " & " + getFirstName(profile.parent2);

		// if just one parent name exists
		} else if (profile.parent1 || profile.parent2) {
			return getFirstName(profile.parent1) || getFirstName(profile.parent2);

		// if neither exists, use null (mustache will not render)
		} else {
			return null;
		}
	}
}

// get the first name in a given full name
function getFirstName(name) {
	if (name) {
		// split into components
		var components = name.split(' ');

		// return first non-empty component
		for (var i = 0; i < components.length; i++) {
			if (components[i] != "") {
				return components[i];
			}
		}

		return null;
	} else {
		return null;
	}
}