# HCA Attendance System :: Web Application

A web application to allow the Honduras Child Alliance to manage its attendance data.

**Created by St. Anne's-Belfield Honors Software Engineering 2019** - Thomas Castleman, Stewart Morris, Jack Riley, Anders Knospe, Shihan Liu, Eddy Chen, & Rafe Forward

**Dedicated to Gus Tingley**

## Features

The web application component of this system allows authorized teachers and administrators to view stats on attendance data, discipline, absence, and snack consumption. They are also able to search & edit attendance reports and student profiles, add new students, and maintain system parameters.

## Authentication

Authentication through Google OAuth 2.0 (with [passport](https://www.npmjs.com/package/passport)) is used to ensure that only users with a valid Google account that matches an existing user account in the database are allowed to use the service.

## Installation

Project dependencies should be installed by running `npm install` in the repository directory. 

Before `server.js` can be run, the database must be built. To do this, log into MySQL and run `SOURCE create_db.sql;`.

This will configure the database.

A credentials file named `credentials.js` must exist in the repository directory, with the following format:

```


/*
	credentials.js: Sensitive information
*/

module.exports = {
	// session secret
	SESSION_SECRET: "<YOUR SESSION SECRET HERE>",

	// Google OAuth credentials
	GOOGLE_CLIENT_ID: "<YOUR GOOGLE CLIENT ID HERE>",
	GOOGLE_CLIENT_SECRET: "<YOUR GOOGLE CLIENT SECRET HERE>",

	// mobile app client ID for authenticating requests from mobile
	MOBILE_CLIENT_ID: "<YOUR MOBILE APP GOOGLE CLIENT ID HERE>",

	// service domain
	domain: "<YOUR DOMAIN NAME HERE>",

	// db credentials
	MySQL_username: "<YOUR MYSQL USERNAME HERE>",
	MySQL_password: "<YOUR MYSQL USER PASSWORD HERE>"
}

```

This will allow the system to access Google APIs as well as run database queries with the user provided. The Mobile Client ID must be the client ID of the [mobile application](https://bitbucket.org/honduras-ca/hca-mobile-app/src/master/) which interacts with this web application to sync reports to the web. Having this credential allows the web application to verify that requests from a mobile device are authentic.

You should ensure the MySQL user provided in `credentials.js` exists and has been granted privileges on the database `HCA_attendance`.

Now, the server may be run with `node server.js`

The console should display something similar to

```
HCA Attendance server listening on port 8080

```