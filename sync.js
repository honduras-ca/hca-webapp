
/*
	sync.js: Functionality for syncing the info from a mobile device with the master web database
*/

var con 			= require('./database.js').connection;
var sys 			= require('./systemSettings.js');
var Levenshtein 	= require('levenshtein');

module.exports = {

	// field an incoming sync request, update DB, and respond appropriately
	handleSync: function(req, res) {
		var m = module.exports;	// shorten the name of module.exports to "m"

		// pull content from request, replacing with empty array if undefined
		var newStudents = req.body.newStudents ? req.body.newStudents : [];
		var modifiedStudents = req.body.modifiedStudents ? req.body.modifiedStudents : [];
		var attendanceReports = req.body.attendanceReports ? req.body.attendanceReports : [];
		var individualRecords = req.body.individualRecords ? req.body.individualRecords : [];
		var teacherReportLink = req.body.teacherReportLink ? req.body.teacherReportLink : [];

		// parse newStudents to ensure none of them already exist in DB
		m.parseNewStudents(newStudents, modifiedStudents, function(err, parsedNewStudents, parsedModifiedStudents) {
			if (!err) {
				// attempt to insert the new students
				m.insertStudents(parsedNewStudents, function(err, firstNewStudentID, numStudentsInserted) {
					if (!err) {
						// get a mapping of local to global UIDs of each new student
						var stuGlobalUID = mapStudentUIDs(newStudents, firstNewStudentID);

						// attempt to apply the student modifications to the DB
						m.applyModifications(parsedModifiedStudents, function(err) {
							if (!err) {
								// attempt to insert new attendance reports, getting ID of first inserted
								m.insertAttendanceReports(attendanceReports, function(err, firstNewReportUID, numReportsInserted) {
									if (!err) {
										// get mapping of local to global UIDs of each attendance report
										var reportGlobalUID = mapReportUIDs(attendanceReports, firstNewReportUID);

										// remap all the referenced UIDs in individual records and teacher report links to the new global UIDs in web DB
										remapIndividualRecordUIDs(individualRecords, stuGlobalUID, reportGlobalUID);
										remapTeacherReportLinkUIDs(teacherReportLink, reportGlobalUID);

										// insert individual records
										m.insertIndividualRecords(individualRecords, function(err) {
											if (!err) {
												// insert teacher report links
												m.insertTeacherReportLinks(teacherReportLink, function(err) {
													if (!err) {
														// construct a response
														m.constructResponseObject(function(err, obj) {
															// respond with any error and the db object
															res.send({ error: err, data: obj });
														});
													} else {
														res.send({ error: err });
													}
												});
											} else {
												res.send({ error: err });
											}
										});
									} else {
										res.send({ error: err });
									}
								});
							} else {
								res.send({ error: err });
							}
						});
					} else {
						res.send({ error: err });
					}
				});
			} else {
				res.send({ error: err});
			}
		});
	},

	// determine which new students already exist in DB, format them into modification objects, callback on parsed lists of newStudents and modifiedStudents
	parseNewStudents: function(newStudents, modifiedStudents, callback) {
		// get all existing students to check for duplicates in new students array
		con.query('SELECT * FROM students;', function(err, rows) {
			if (!err && rows !== undefined) {
				// create map of new student names to existing profiles that potentially match that new student
				var potentialMatches = {};

				// iterate over existing students
				for (var i = 0; i < rows.length; i++) {
					var r = rows[i], existingName = clean(rows[i].name);

					// check existing name against name of each new student
					for (var j = 0; j < newStudents.length; j++) {
						var newName = clean(newStudents[j].name);

						// if name edit distance below threshold
						if (editDist(existingName, newName) <= sys.syncLevEquivThreshold) {
							// add existing student record to potential matches for new student
							if (potentialMatches[newName] == null) {
								potentialMatches[newName] = [r];
							} else {
								potentialMatches[newName].push(r);
							}
						}
					}
				}

				// list of students that are verifiably not already in the web DB
				var actuallyNewStudents = [];

				// for each new student from mobile
				for (var i = 0; i < newStudents.length; i++) {
					var newS = newStudents[i];
					var existing = potentialMatches[clean(newS.name)];

					// if there are students with same name
					if (existing != null && existing.length > 0) {
						var existsAlready = false;

						// for each existing student with same name
						for (var j = 0; j < existing.length; j++) {
							var existingS = existing[j];

							// check equivalence of the two student profiles with same name
							if (checkProfileEquivalency(newS, existingS)) {
								// register that "new" student actually already is on record
								existsAlready = true;

								// register this "new" student's global UID as that of its pre-existing counterpart
								newS.globalUID = existingS.uid;

								// create a new modification object for this student
								var mod = {
									"uid": existingS.uid,
									"name": { value: existingS.name, isChanged: false },	// never changed, since we already know the names are the same
									"parent1": modField(existingS.parent1, newS.parent1),
									"parent2": modField(existingS.parent2, newS.parent2),
									"isAdult": modField(existingS.isAdult, newS.isAdult),
									"address": modField(existingS.address, newS.address),
									"phone": modField(existingS.phone, newS.phone),
									"specialCircumstances": modField(existingS.specialCircumstances, newS.specialCircumstances),
									"uniformSize": modField(existingS.uniformSize, newS.uniformSize),
								}

								// add this mod object to the list of modified students
								modifiedStudents.push(mod);
							}
						}

						// if none of the students with same name are equivalent to this student
						if (!existsAlready) {
							// add to new list
							actuallyNewStudents.push(newS);
						}

					} else {
						// if no existing student with same name, we definitely know this profile is new
						actuallyNewStudents.push(newS);
					}
				}

				// callback on the new list of new students and the updated list of modified students
				callback(err, actuallyNewStudents, modifiedStudents);

			} else {
				callback(err);
			}
		});
	},

	// batch insert a list of new student objects, callback on the ID of the first insert and the number of rows inserted
	insertStudents: function(students, callback) {
		// if there are students to insert
		if (students && students.length > 0) {
			var formattedStudents = [];

			// format each student object
			for (var i = 0; i < students.length; i++) {
				var s = students[i];

				// add new student in array format: [name, parent1, parent2, isAdult, address, phone, specialCircumstances, uniformSize]
				formattedStudents.push([s.name, s.parent1, s.parent2, s.isAdult, s.address, s.phone, s.specialCircumstances, s.uniformSize]);
			}

			// attempt batch insert and get last insert ID and row count
			con.query('INSERT INTO students (name, parent1, parent2, isAdult, address, phone, specialCircumstances, uniformSize) VALUES ?; SELECT LAST_INSERT_ID() AS firstInsertedID, ROW_COUNT() AS rowCount;', [formattedStudents], function(err, rows) {
				if (!err && rows !== undefined && rows.length > 1 && rows[1].length > 0) {
					// callback on ID of first insert, and number of rows successfully inserted
					callback(err, rows[1][0].firstInsertedID, rows[1][0].rowCount)
				} else {
					// callback with error
					callback(err);
				}
			});
		} else {
			callback();
		}
	},

	// attempt to apply the modifications outlined in modifiedStudents to the students table
	applyModifications: function(modifiedStudents, callback) {
		// if there are modifications to apply
		if (modifiedStudents && modifiedStudents.length > 0) {
			// fields to be used in UPDATE queries, in specific order
			var fields = ['name', 'parent1', 'parent2', 'isAdult', 'address', 'phone', 'specialCircumstances', 'uniformSize'];

			// concatenation of UPDATE queries to apply
			var fullStatement = "";

			// list of modifications formatted in a single array in prepared statement format
			var formattedMods = [];

			// for each modified student
			for (var i = 0; i < modifiedStudents.length; i++) {
				var mod = modifiedStudents[i];

				// start query with basic UPDATE syntax
				var query = "UPDATE students SET ";

				// for each modifiable field
				for (var f = 0; f < fields.length; f++) {
					var obj = mod[fields[f]];

					// if field exists in modification object and a change has occurred
					if (obj && obj.isChanged) {
						// if special circumstances modification, only allow to append
						if (fields[f] == "specialCircumstances") {
							// add new value to query array, with space to separate from previous circumstances
							formattedMods.push(" " + obj.value);

							// concatenate the current circumstances with new circumstances
							query += "specialCircumstances = CONCAT(IFNULL(specialCircumstances, \"\"), ?),";
						} else {
							// add new value to query array
							formattedMods.push(obj.value);

							// add to query to specify modification of this field
							query += fields[f] + " = ?,";
						}
					}
				}

				// trim off the last character, either the last comma (problematic), or if nothing added, a space
				query = query.substring(0, query.length - 1);

				// close out query with identifier for student
				query += " WHERE uid = ?;";

				// if query actually contains modifications
				if (query != "UPDATE students SET WHERE uid = ?;") {
					// add student UID to end of this entry in formatted array so query can UPDATE this specific student
					formattedMods.push(mod.uid);

					// add another update query to the full statement
					fullStatement += query;
				}
			}

			// ensure a statement is being run
			if (fullStatement != "") {
				// apply all UPDATE queries in one connection
				con.query(fullStatement, formattedMods, function(err) {
					callback(err);
				});
			} else {
				callback();
			}
		} else {
			callback();
		}
	},

	// batch insert a list of attendance reports, callback on the ID of the first insert and number of rows inserted
	insertAttendanceReports: function(attendanceReports, callback) {
		// if there are attendance reports to add
		if (attendanceReports && attendanceReports.length > 0) {
			var formattedReports = [];

			// format each attendance report object into prepared statement format
			for (var i = 0; i < attendanceReports.length; i++) {
				var r = attendanceReports[i];

				// add new report in array format: [classroomUID, classTypeUID, snackUID, whenRecorded, comments]
				formattedReports.push([r.classroomUID, r.classTypeUID, r.snackUID, r.whenRecorded, r.comments]);
			}

			// attempt the batch insert, retrieving first inserted ID and row count
			con.query('INSERT INTO attendanceReports (classroomUID, classTypeUID, snackUID, whenRecorded, comments) VALUES ?; SELECT LAST_INSERT_ID() AS firstInsertedID, ROW_COUNT() AS rowCount;', [formattedReports], function(err, rows) {
				if (!err && rows !== undefined && rows.length > 1 && rows[1].length > 0) {
					// callback on ID of first insert, and number of rows successfully inserted
					callback(err, rows[1][0].firstInsertedID, rows[1][0].rowCount)
				} else {
					// callback with error
					callback(err);
				}
			});
		} else {
			callback();
		}
	},

	// batch insert the individual records
	insertIndividualRecords: function(individualRecords, callback) {
		// if there are individual records to insert
		if (individualRecords && individualRecords.length > 0) {
			var formattedRecords = [];

			// format each individual record object into prepared statement format
			for (var i = 0; i < individualRecords.length; i++) {
				var r = individualRecords[i];

				// add new record in array format: [reportUID, studentUID, didEat, disciplinePoints, comments]
				formattedRecords.push([r.reportUID, r.studentUID, r.didEat, r.disciplinePoints, r.comments]);
			}

			// attempt the batch insert
			con.query('INSERT INTO individualRecords (reportUID, studentUID, didEat, disciplinePoints, comments) VALUES ?;', [formattedRecords], function(err) {
				callback(err);
			});
		} else {
			callback();
		}
	},

	// batch insert the teacher report links
	insertTeacherReportLinks: function(teacherReportLink, callback) {
		// if there are teacher-report links to add
		if (teacherReportLink && teacherReportLink.length > 0) {
			var formattedRecords = [];

			// format each individual record object into prepared statement format
			for (var i = 0; i < teacherReportLink.length; i++) {
				var l = teacherReportLink[i];

				// add new record in array format: [reportUID, studentUID, didEat, disciplinePoints, comments]
				formattedRecords.push([l.reportUID, l.teacherUID]);
			}

			// attempt the batch insert
			con.query('INSERT INTO teacherReportLink (reportUID, teacherUID) VALUES ?;', [formattedRecords], function(err) {
				callback(err);
			});
		} else {
			callback();
		}
	},

	// construct a response to the sync request, pulling from latest versions of necessary tables
	constructResponseObject: function(callback) {
		// construct query for selecting data from each necessary table
		var query = [
			'SELECT * FROM classrooms;',
			'SELECT * FROM classTypes;',
			'SELECT * FROM snacks;',
			'SELECT * FROM students;',
			'SELECT uid, email, name, isAdmin FROM users;'
		];

		// run large query, selecting everything in one connection
		con.query(query.join(''), function(err, rows) {
			if (!err && rows !== undefined && rows.length >= query.length) {
				// construct response objects
				var response = {
					classrooms: rows[0],
					classTypes: rows[1],
					snacks: rows[2],
					students: rows[3],
					users: rows[4]
				};

				callback(err, response);
			} else {
				callback(err, null);
			}
		});
	}

}

// determine whether or not two separate student profiles (web and mobile) are describing the same student
function checkProfileEquivalency(a, b) {
	// perform some string cleaning on parents of profiles A and B and cache results
	var Ap1 = cleanParent(a.parent1), Ap2 = cleanParent(a.parent2);
	var Bp1 = cleanParent(b.parent1), Bp2 = cleanParent(b.parent2);

	// calculate edit distance between cleaned names
	var nameDist = editDist(clean(a.name), clean(b.name));

	// get edit distance between all possible parent pairings
	var ed11 = editDist(Ap1, Bp1);
	var ed22 = editDist(Ap2, Bp2);
	var ed12 = editDist(Ap1, Bp2);
	var ed21 = editDist(Ap2, Bp1);

	var pair1Dist, pair2Dist;

	// only consider the parent pairing with minimum overall edit distance
	if (ed11 + ed22 < ed12 + ed21) {
		pair1Dist = ed11;
		pair2Dist = ed22;
	} else {
		pair1Dist = ed12;
		pair2Dist = ed21;
	}

	// if name distance and parent pairs distance below threshold, profiles are equivalent
	return nameDist <= sys.syncLevEquivThreshold && pair1Dist <= sys.syncLevEquivThreshold && pair2Dist <= sys.syncLevEquivThreshold;
}

// get the edit distance between two strings
function editDist(compareTo, baseItem) {
  return new Levenshtein(compareTo, baseItem).distance;
}

// remove capitalization and strip string of any extra whitespace
function clean(s) {
	// if string actually exists
	if (s) {
		var sp = s.toLowerCase().split(/\s/g);
		var clean = [];

		// add component to final string only if non-empty
		for (var i = 0; i < sp.length; i++) {
			if (sp[i] != '') {
				clean.push(sp[i]);
			}
		}

		// join all components with spaces
		return clean.join(' ');
	} else {
		// return empty string
		return '';
	}
}

// remove capitalization and extract only first name of parent
function cleanParent(s) {
	// if string actually exists
	if (s) {
		var sp = s.toLowerCase().split(/\s/g);

		// add component to final string only if non-empty
		for (var i = 0; i < sp.length; i++) {
			if (sp[i] != '') {
				return sp[i];
			}
		}

		return '';
	} else {
		return '';
	}
}

// construct a {value, isChanged} object for a given field, based on the previous and new values of an existing student and a "new" student
function modField(existingValue, newValue) {
	// take the new value only if the existing value 
	return { value: existingValue == null ? newValue : existingValue, isChanged: existingValue == null && newValue != null }
}

// get a mapping of student local UIDs to their global UIDs, all whilst applying these global UIDs to the student objects
function mapStudentUIDs(newStudents, firstID) {
	var stuGlobalUID = {};

	// ensure first insert ID exists
	if (firstID != null) {
		// for each new student
		for (var i = 0; i < newStudents.length; i++) {
			// if student has no global UID yet (they are an actual new student)
			if (newStudents[i].globalUID == null) {
				// add ID, starting from first inserted ID and incrementing for each inserted student
				newStudents[i].globalUID = firstID++;
			}

			// associate this student's local UID with its global UID
			stuGlobalUID[newStudents[i].uid] = newStudents[i].globalUID;
		}
	}

	return stuGlobalUID;
}

// get a mapping of report local UIDs to their global UIDs, applying global UIDs to report objects
function mapReportUIDs(attendanceReports, firstID) {
	var reportGlobalUID = {};

	// if first insert ID exists
	if (firstID != null) {
		// for each new attendance report
		for (var i = 0; i < attendanceReports.length; i++) {
			// add ID, starting from first inserted ID and incrementing for each inserted report
			attendanceReports[i].globalUID = firstID++;

			// associate this report's local UID with its global UID
			reportGlobalUID[attendanceReports[i].uid] = attendanceReports[i].globalUID;
		}
	}

	return reportGlobalUID;
}

// replace all referenced local UIDs in individual records with global counterparts
function remapIndividualRecordUIDs(individualRecords, studentIDMapping, reportIDMapping) {
	// for each individual record
	for (var i = 0; i < individualRecords.length; i++) {
		var r = individualRecords[i];

		// map student UID if mapping exists (otherwise, not new student and no change necessary)
		r.studentUID = studentIDMapping[r.studentUID] != null ? studentIDMapping[r.studentUID] : r.studentUID;

		// map report UID
		r.reportUID = reportIDMapping[r.reportUID];
	}
}

// replace all referenced local UIDs in teacher-report links with global counterparts
function remapTeacherReportLinkUIDs(teacherReportLinks, reportIDMapping) {
	// for each link
	for (var i = 0; i < teacherReportLinks.length; i++) {
		teacherReportLinks[i].reportUID = reportIDMapping[teacherReportLinks[i].reportUID];		// map report UID
	}
}