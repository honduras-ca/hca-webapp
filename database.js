
/*
	database.js: MySQL connection to database object
*/

var creds = require('./credentials.js');
var mysql = require('mysql');

module.exports = {
	connection: mysql.createPool({
	    host: 'localhost',
	    user: creds.MySQL_username,
	    password: creds.MySQL_password,
	    database: 'HCA_attendance',
	    multipleStatements: true
	})
}