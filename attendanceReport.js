
/*
	attendanceReport.js: Functionality for getting, editing, and searching attendance reports
*/

var con = require('./database.js').connection;
var moment = require('moment');
var sys = require('./systemSettings.js');

module.exports = {

	/*	Gets all the data associated with a given attendance report, including individual records of students in that report.
		Callback on error and all report data */
	getAttendanceReport: function(reportUID, callback) {
		// ensure given report UID exists
		if (reportUID) {
			// get all data for this report (JOIN to get friendly classroom, classtype, and snack)
			con.query('SELECT a.*, r.name AS classroom, t.name AS classType, t.isAdult AS isAdultClass, s.name AS snack FROM attendanceReports a LEFT JOIN classrooms r ON a.classroomUID = r.uid LEFT JOIN classTypes t ON a.classTypeUID = t.uid LEFT JOIN snacks s ON a.snackUID = s.uid WHERE a.uid = ?;', [reportUID], function(err, rows) {
				if (!err && rows !== undefined && rows.length > 0) {
					// get report object
					var report = rows[0];

					// parse when recorded date from report
					var parsedDate = moment(report.whenRecorded);

					// if parse successful
					if (parsedDate.isValid()) {
						// format date recorded appropriately, add to report object
						report.formattedWhenRecorded = parsedDate.format('MMMM Do, YYYY [at] h:mm A');
					}

					// pull individual records from DB as well (JOIN to get student name)
					con.query('SELECT i.*, s.name AS student FROM individualRecords i JOIN students s ON i.studentUID = s.uid WHERE reportUID = ?;', [reportUID], function(err, rows) {
						if (!err && rows !== undefined) {
							// add individual records to report object
							report.records = rows;

							// get the name of each teacher linked to this report
							con.query('SELECT u.name, u.email FROM teacherReportLink l JOIN users u ON l.teacherUID = u.uid WHERE l.reportUID = ?;', [reportUID], function(err, rows) {
								if (!err && rows !== undefined) {
									report.teachers = rows;

									// extract names of each associated teacher
									var names = [];
									for (var i = 0; i < rows.length; i++) {
										// record name, or email if no name given
										names.push(rows[i].name || rows[i].email);
									}

									// join teacher names with commas for teacher string
									report.teacherString = names.join(", ");

									// callback on report information
									callback(err, report);
								} else {
									callback("Unable to find teachers linked to attendance report.");
								}
							});
						} else {
							callback("Unable to find records under this report.");
						}
					});
				} else {
					// callback on DB error or custom error
					callback(err || "No report found.");
				}
			});
		} else {
			callback("No report UID given.");
		}
	},

	/*	Remove a given attendance report from the db
		Callback on error */
	deleteAttendanceReport: function(reportUID, callback) {
		// if report UID is defined
		if (reportUID) {
			// run delete query
			con.query('DELETE FROM attendanceReports WHERE uid = ?;', [reportUID], function(err) {
				callback(err);
			});
		} else {
			// notify of error that no report UID was given
			callback("No report UID given.");
		}
	},

	/*	Change the fields of a given attendance report, only if it was reported within a cutoff timeframe (systemSettings). Cannot change the whenRecorded field.
		Callback on error and UID of report */
	editAttendanceReport: function(updatedReport, callback) {
		// ensure edited report object exists
		if (updatedReport) {
			// pull existing report / individual record info
			con.query('SELECT * FROM attendanceReports WHERE uid = ?;', [updatedReport.uid], function(err, rows) {
				if (!err && rows !== undefined && rows.length > 0) {
					var existingReport = rows[0];

					// if within editing timeframe of this report
					if (checkWithinEditingTimeframe(existingReport.whenRecorded)) {
						// construct new report, taking definitions from edited report when possible
						var newReport = Object.assign(existingReport, updatedReport);

						// run update on existing report
						con.query('UPDATE attendanceReports SET classroomUID = ?, classTypeUID = ?, snackUID = ?, comments = ? WHERE uid = ?;', [newReport.classroomUID, newReport.classTypeUID, newReport.snackUID, newReport.comments, existingReport.uid], function(err) {
							callback(err, existingReport.uid);
						});
					} else {
						// callback on error for editing past timeframe
						callback("Unable to edit this report: Past the editing timeframe.")
					}
				} else {
					callback(err || "No report found.");
				}
			});
		} else {
			callback("No edits given.");
		}
	},

	/*	Remove a given individual record from the db
		Callback on error */
	deleteIndividualRecord: function(recordUID, callback) {
		// if record UID is defined
		if (recordUID) {
			// run delete query
			con.query('DELETE FROM individualRecords WHERE uid = ?;', [recordUID], function(err) {
				callback(err);
			});
		} else {
			// notify of error that no record UID was given
			callback("No record UID given.");
		}
	},

	/*	Change the fields of a given individual record, only if within editing timeframe.
		Callback on error and uid of parent report */
	editIndividualRecord: function(updatedRecord, callback) {
		// ensure edited record exists
		if (updatedRecord) {
			// get parent attendance report
			con.query('SELECT i.*, a.whenRecorded FROM attendanceReports a JOIN individualRecords i ON a.uid = i.reportUID WHERE i.uid = ?;', [updatedRecord.uid], function(err, rows) {
				if (!err && rows !== undefined && rows.length > 0) {
					var record = rows[0];

					// if within editing timeframe for this report
					if (checkWithinEditingTimeframe(record.whenRecorded)) {
						// apply edits
						var newRecord = Object.assign(record, updatedRecord);

						// apply update of individual record to DB
						con.query('UPDATE individualRecords SET studentUID = ?, didEat = ?, disciplinePoints = ?, comments = ? WHERE uid = ?;', [newRecord.studentUID, newRecord.didEat, newRecord.disciplinePoints, newRecord.comments, record.uid], function(err) {
							callback(err, record.reportUID);
						});
					} else {
						callback("Unable to edit this record: Past the editing timeframe.");
					}
				} else {
					callback("Unable to find parent report.");
				}
			});
		} else {
			callback("No edits given.");
		}
	},

	/*	Get the past N attendance reports which match this classroom and class type, ordered by recentness.
		Callback on error and reports array containing (uid, classroom, classType, whenRecorded) */
	searchAttendanceReports: function(startDate, endDate, classroomUID, classTypeUID, numReports, callback) {
		// ensure required fields exist to search
		if (numReports != null && startDate && endDate && startDate.isValid() && endDate.isValid()) {
			// ensure valid date range
			if (startDate.isBefore(endDate) || startDate.isSame(endDate)) {
				// format dates into strings for query, and make inclusive range
				var startString = startDate.format('YYYY-MM-DD');
				var endString = endDate.add(1, 'days').format('YYYY-MM-DD');

				// search for the last N reports matching this information (JOIN to get friendly names of classroom and classtype from UIDs)
				con.query('SELECT a.uid, r.name AS classroom, t.name AS classType, a.whenRecorded FROM attendanceReports a LEFT JOIN classrooms r ON a.classroomUID = r.uid LEFT JOIN classTypes t ON a.classTypeUID = t.uid WHERE (a.classroomUID = ? OR ? IS NULL) AND (a.classTypeUID = ? OR ? IS NULL) AND (a.whenRecorded BETWEEN ? AND ?) ORDER BY a.whenRecorded DESC LIMIT ?;', [classroomUID, classroomUID, classTypeUID, classTypeUID, startString, endString, numReports], function(err, rows) {
					if (!err && rows !== undefined) {

						// format date of each report properly
						for (var i = 0; i < rows.length; i++) {
							rows[i].whenRecorded = moment(rows[i].whenRecorded).format('MMMM Do, YYYY [at] h:mm A');
						}

						// callback on array of report objects
						callback(err, rows);
					} else {
						// callback on error finding matching reports
						callback(err || "No reports found.");
					}
				});
			} else {
				// error on start date being after end date
				callback("Start date must be before end date in reports search.");
			}
		} else {
			// error on lack of required fields
			callback("Not all required search fields were given.");
		}
	},

	/*	Check if an attendance report is within its editing timeframe.
		Callback on error boolean isEditable */
	isEditable: function(reportUID, callback) {
		if (reportUID) {
			// get the recorded date of this report
			con.query('SELECT whenRecorded FROM attendanceReports WHERE uid = ?;', [reportUID], function(err, rows) {
				if (!err && rows !== undefined && rows.length > 0) {
					// parse date when report recorded
					var recordDate = moment(rows[0].whenRecorded);

					// if successfully parsed
					if (recordDate && recordDate.isValid()) {
						// add report edit window duration to record date to get last possible day of editing
						var cutoff = recordDate.add(sys.reportUpdateWindow);

						// if before cutoff
						if (moment().isBefore(cutoff)) {
							// report is editable
							callback(err, true);
						} else {
							// report is NOT editable
							callback(err, false);
						}
					} else {
						// error on invalid date
						callback("Invalid recorded date for report.");
					}
				} else {
					// error on lack of reports found
					callback(err || "No report found.");
				}
			});
		} else {
			// error on null input
			callback("No report UID given.");
		}
	},

	/*	Replace the records in teacherReportLink associated with a given report
		Callback on error */
	updateTeachers: function(reportUID, teacherUIDs, callback) {
		// ensure report UID and teacher UIDs are defined
		if (reportUID && teacherUIDs) {
			var insertValues = [];

			// construct array of insert values
			for (var i = 0; i < teacherUIDs.length; i++) {
				insertValues.push([teacherUIDs[i], reportUID]);
			}

			// remove existing links
			con.query('DELETE FROM teacherReportLink WHERE reportUID = ?;', [reportUID], function(err) {
				if (!err) {
					// add new teachers, if any
					if (teacherUIDs.length > 0) {
						// add new links to relation table
						con.query('INSERT INTO teacherReportLink (teacherUID, reportUID) VALUES ?;', [insertValues], function(err) {
							callback(err);
						});
					} else {
						callback(err);
					}
				} else {
					callback(err);
				}
			});
		} else {
			// callback on error with null values
			callback("Must have report UID and teacher UIDs");
		}
	}

}

// determine whether or not it is possible to edit a given attendance report based on when it was recorded
function checkWithinEditingTimeframe(whenRecorded) {
	// parse date of attendance report
	var recorded = moment(whenRecorded);

	// if whether or not before editing cutoff
	return whenRecorded && recorded.isValid() && moment().isBefore(recorded.add(sys.reportUpdateWindow));
}