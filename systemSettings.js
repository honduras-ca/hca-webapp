
/*
	systemSettings.js: Important system variables
*/

var moment = require('moment');

module.exports = {
	// port used for development
	DEV_PORT: 8080,

	// if system is in development mode (more explicit error logging, no https) (turn off for deployment)
	inDevMode: false,

	// max edit distance between student names (& parent names) for profiles to be considered equivalent
	syncLevEquivThreshold: 2,

	// window within which editing of attendance reports on the web is still possible
	reportUpdateWindow: moment.duration(1, 'week'),

	// options for timeframe selection and their abbreviations
	timeframes: {
		"1d": { abbrev: "1d", name: "1 Day", duration: moment.duration(1, 'days') },
		"3d": { abbrev: "3d", name: "3 Days", duration: moment.duration(3, 'days') },
		"1w": { abbrev: "1w", name: "1 Week", duration: moment.duration(1, 'weeks') },
		"2w": { abbrev: "2w", name: "2 Weeks", duration: moment.duration(2, 'weeks') },
		"1m": { abbrev: "1m", name: "1 Month", duration: moment.duration(1, 'months') },
		"3m": { abbrev: "3m", name: "3 Months", duration: moment.duration(3, 'months') },
		"6m": { abbrev: "6m", name: "6 Months", duration: moment.duration(6, 'months') },
		"A": { abbrev: "A", name: "All Time" }
	},

	// default timeframe for homepage
	defaultHomePageTimeframeAbbrev: '2w',

	// default timeframe for disciplinary history on student page
	defaultDiscHistoryTimeframe: '2w',

	// how many students are listed in the Potential Attendance Issues section of homepage
	numAbsentStudentsDisplayed: 10,

	// how many class days can a student miss before being listed in the Potential Attendance Issues section of homepage
	numDaysAbsent: 5,

	// how many students are listed in the Potential Disciplinary Issues section of homepage
	numDisciplinaryIssuesDisplayed: 10,

	// number of snacks to display in the homepage bar chart of snack approval
	numSnacks: 5,

	// max number of search results for students
	numStudentSearchResults: 50,

	// max number of search results for reports
	numReportSearchResults: 50,

	// maximum number of disciplinary points a student can receive
	maxDiscPoints: 3,

	// maximum number of data points to display in a Chart.js chart (before it looks bad)
	maxNumChartPoints: 65
}
