var express 			= require('express');
var app 			= express();
var mustacheExpress		= require('mustache-express');
var bodyParser 			= require('body-parser');
var cookieParser 		= require('cookie-parser');
var session 			= require('cookie-session');
var passport 			= require('passport');
var creds			= require('./credentials.js');
var morgan			= require('morgan');
var fs				= require('fs');

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.engine('html', mustacheExpress());
app.set('views', __dirname + '/views');
app.use(express.static(__dirname + '/static'));
app.use(express.static(__dirname + '/views'));
// app.use(morgan('dev'));

// configure session
app.use(session({
	secret: creds.SESSION_SECRET,
	name: 'session',
	resave: true,
	saveUninitialized: true
}));

var auth = require('./auth.js').init(app, passport);	// initialize authentication system
var sys = require('./systemSettings.js');				// require system settings file
var routes = require('./routes.js').init(app);			// initialize routes

app.get('*', function(req, res) {
	res.redirect('/');
});

// if production server, use https
if (!sys.inDevMode) {
	var lex = require('greenlock-express').create({
	 	server: 'https://acme-v02.api.letsencrypt.org/directory',
		version: 'draft-11',
		email: creds.greenlockEmail,
		agreeTos: true,
		approvedDomains: creds.approvedDomains,
		communityMember: true,
		store: require('greenlock-store-fs')
	});

	require('http').createServer(lex.middleware(require('redirect-https')())).listen(80, function () {
		console.log("HCA service listening on", this.address().port);
	});

	require('https').createServer(lex.httpsOptions, app).listen(443, function() {
		console.log("HCA service listening on", this.address().port);
	});
} else {
	// set up dev server
	var server = app.listen(sys.DEV_PORT, function() {
		console.log('Development HCA server listening on port %d', server.address().port);
	});
}
