
/*
	tools.js: Functionality for admin / teacher updates to the system (authorizing accounts, adding / deleting classrooms, class types, etc)
*/

var con = require('./database.js').connection;

module.exports = {

	/*	Add a new user account by entering an email into the users table with a given admin status. 
		The user’s name will be pulled when they authenticate with their Google account.
		If user already exists, their isAdmin attribute will be updated.
		Callback on: error, if any. */
	authorizeUser: function(email, name, isAdmin, callback) {
		// make sure they enter an email
		if (email != null && name != null && isAdmin != null) {
			// add new admin user, if already exists, make admin
			con.query('INSERT INTO users (email, name, isAdmin) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE isAdmin = ?;', [email, name, isAdmin, isAdmin], callback);
		} else {
			// no email entered
			callback("Null field (email, name or isAdmin)");
		}
	},

	/*	Remove a user profile from the DB, admin or teacher.
			If teacher, they will be effectively unlinked from any past class reports.
			For deauthorizing without removing linkage & history of account, see archival.
		Callback on: error, if any. */
	deauthorizeUser: function(uid, callback) {
		// ensure UID is defined before deleting user
		if (uid != null) {
			// remove user from users table
			con.query('DELETE FROM users WHERE uid = ?;', [uid], callback);
		} else {
			// callback on null UID error
			callback("Unable to deauthorize user as no UID was given.");
		}
	},

	/*	Update the archival status of a user account (teacher or admin).
			If a user is archived, they will not be able to access the system
			but any trace of their previous activity will remain */
	updateArchiveStatus: function(uid, status, callback) {
		if (uid && (status == 1 || status == 0)) {
			con.query('UPDATE users SET isArchived = ? WHERE uid = ?;', [status, uid], callback);
		} else {
			callback("Unable to update archive status as not all fields were properly supplied.");
		}
	},

	/*	Add a new classroom name into the classrooms table to be used by teachers when recording attendance.
		Callback on: error, if any */
	createClassroom: function(name, callback) {
		// ensure the name exists
		if (name != null) {
			// add a new classroom
			con.query('INSERT INTO classrooms (name) VALUES (?);', [name], callback);
		} else {
			// they wacked it, no name was given
			callback("No classroom name given.");
		}
	},

	/*	Delete an existing classroom from the system. All references to this get assigned to the “Unknown” classroom.
		Callback on: error, if any */
	deleteClassroom: function(uid, callback) {
		// ensure UID exists and is not Unknown UID (1)
		if (uid != null && uid != 1) {
			// update the classroom to "Unknown" (UID 1) in all attendance reports that referenced this classroom
			con.query('UPDATE attendanceReports SET classroomUID = 1 WHERE classroomUID = ?;', [uid], function(err) {
				if (!err) {
					// remove the entry from classrooms table now that references have been reset
					con.query('DELETE FROM classrooms WHERE uid = ?;', [uid], callback);
				} else {
					callback(err);
				}
			});
		} else {
			// no uid entered
			callback("Unable to delete classroom with provided UID (" + uid + ").");
		}
	},

	/*	Add a new class type into the classTypes table to be used by teachers when recording attendance.
		Callback on: error, if any */
	createClassType: function(name, isAdult, callback) {
		// ensure name and adult status exist
		if (name && isAdult) {
			// add a new class type
			con.query('INSERT INTO classTypes (name, isAdult) VALUES (?, ?);', [name, isAdult], callback);
		} else {
			// wacked it, there was not enough info given
			callback("Insufficient information given to create a new class type.");
		}
	},

	/*	Delete an existing class type from the system. All references to this get assigned to the “Unknown” class type.
		Callback on: error, if any */
	deleteClassType: function(uid, callback) {
		// ensure UID given exists and is not the Unknown UID (1)
		if (uid && uid != 1) {
			// update classType to "Unknown" (UID 1) in all attendance reports that referenced this classType
			con.query('UPDATE attendanceReports SET classTypeUID = 1 WHERE classTypeUID = ?;', [uid], function(err) {
				if (!err) {
					// remove entry from classTypes now that references have been reset
					con.query('DELETE FROM classTypes WHERE uid = ?;', [uid], callback);
				} else {
					callback(err);
				}
			});
		} else {
			// no uid entered
			callback("Unable to delete class type with provided UID (" + uid + ").");
		}
	},

	/* 	Add a new snack type into the snacks table to be used by teachers when recording attendance.
		Callback on: error, if any */
	createSnack: function(name, callback) {
		// ensure snack name exists
		if (name != null) {
			// add new snack option into snacks table
			con.query('INSERT INTO snacks (name) VALUES (?);', [name], callback);
		} else {
			// wacked it, no snack name given
			callback("No snack name given.");
		}
	},

	/*	Delete an existing snack type from the system. All references to this get assigned to the “Unknown” snack type.
		Callback on: error, if any */
	deleteSnack: function(uid, callback) {
		// ensure given UID exists and is not Unknown UID (1)
		if (uid != null && uid != 1) {
			// reset all reference to this snack to "Unknown" (UID 1)
			con.query('UPDATE attendanceReports SET snackUID = 1 WHERE snackUID = ?;', [uid], function(err) {
				if (!err) {
					// remove entry from snacks table now that references have been reset
					con.query('DELETE FROM snacks WHERE uid = ?;', [uid], callback);
				} else {
					callback(err);
				}
			});
		} else {
			// no uid entered
			callback("Unable to delete snack with provided UID (" + uid + ").");
		}
	}
}