
/*
	stats.js: Functionality for calculating organization / class statistics
*/

var con = require('./database.js').connection;
var moment = require('moment');
var sys = require('./systemSettings.js');

module.exports = {

	/*	Get all data needed to populate homepage stats and charts.
		Callback on error and mustache-ready object for page render */
	getAllHomePageStats: function(dateCutoff, classroomUID, classTypeUID, callback) {
		// object which will hold all info needed by homepage frontend
		var result = {};

		// get total attendance counts using given constraints
		module.exports.getAttendanceCounts(dateCutoff, classroomUID, classTypeUID, function(err, counts) {
			if (!err) {
				// store attendance count data in result, and whether or not any data was found
				result.attendanceCounts = module.exports.condenseChartData(counts);
				result.attendanceCountsExist = counts.length > 0;

				// notify render object that counts are averaged (explains float values)
				if (counts.length > sys.maxNumChartPoints) {
					result.averagingAttendanceCounts = true;
				}

				// get absent students using given constraints and system parameters
				module.exports.getAbsentStudents(classroomUID, classTypeUID, sys.numAbsentStudentsDisplayed, sys.numDaysAbsent, function(err, absentees) {
					if (!err) {
						// add absentee data to result, whether or not any exist
						result.absentees = absentees;
						result.absenteesExist = absentees.length > 0;

						// get meta info about classroom and classtype constraints, including whether or not class is adult
						getMetaClassInfo(classroomUID, classTypeUID, function(err, allClasses, roomName, typeName, isAdult) {
							if (!err) {
								// store metadata in result object
								result.allClasses = allClasses;
								result.classroom = roomName;
								result.classType = typeName;
								result.isAdultClass = isAdult;

								// if non-adult class
								if (!isAdult) {
									// get snack consumption information using given constraints
									module.exports.getSnackStats(dateCutoff, classroomUID, classTypeUID, sys.numSnacks, function(err, snacks) {
										if (!err) {
											// add snack data to result object, whether or not snacks exist
											result.snacks = snacks;
											result.snacksExist = snacks.length > 0;

											// get disciplinary data using given constraints
											module.exports.getMeanDisciplinaryPoints(dateCutoff, classroomUID, classTypeUID, sys.numDisciplinaryIssuesDisplayed, function(err, disciplineData) {
												if (!err) {
													// add disciplinary data to result object, whether or not data exists
													result.disciplinaryIssues = disciplineData;
													result.disciplinaryIssuesExist = disciplineData.length > 0;

													// callback on result
													callback(err, result);
												} else {
													// callback on error getting disciplinary stats
													callback(err);
												}
											});
										} else {
											// callback on error getting snack stats
											callback(err);
										}
									});
								} else {
									// callback on result object, no need for snack / disciplinary stats
									callback(err, result);
								}
							} else {
								// callback on error determining whether or not class is adult
								callback(err);
							}
						});
					} else {
						// callback on error getting absent students
						callback(err);
					}
				});
			} else {
				// callback on error getting attendance counts
				callback(err);
			}
		});
	},

	/*	Get the number of students who have attended over a given timeframe, in a given class (by classroom & classType or just classroom). 
		Only use dates upon which class occurred.
		Callback on error and object with array containing each class date with number who attended */
	getAttendanceCounts: function(dateCutoff, classroomUID, classTypeUID, callback) {
		// ensure moment dateCutoff object exists and is valid
		if (dateCutoff && dateCutoff.isValid()) {
			var queryFirstHalf = "SELECT x, COUNT(x) AS y FROM (SELECT i.uid, DATE_FORMAT(a.whenRecorded, \"%Y-%m-%d\") AS x, a.classroomUID, a.classTypeUID FROM individualRecords i JOIN attendanceReports a ON i.reportUID = a.uid WHERE a.whenRecorded > ?";
			var querySecondHalf = ") AS records GROUP BY x ORDER BY x ASC;";
			var extraConditions = "";
			var params = [dateCutoff.format('YYYY-MM-DD')];

			// if classroom search constraint, apply it
			if (classroomUID) {
				// add condition to query
				extraConditions += " AND a.classroomUID = ?";

				// add UID value to query parameters
				params.push(classroomUID);
			}

			// if class type search constraint, apply it
			if (classTypeUID) {
				// add condition to query
				extraConditions += " AND a.classTypeUID = ?";

				// add UID value to query parameters
				params.push(classTypeUID);
			}

			// construct query with conditions
			var query = queryFirstHalf + extraConditions + querySecondHalf + ";";
			
			// get attendance counts
			con.query(query, params, function(err, attendanceCounts) {
				if (!err && attendanceCounts !== undefined) {
					// callback on attendance count data
					callback(err, attendanceCounts);
				} else {
					callback(err || "A problem occurred finding attendance records.");
				}
			});
	 	} else {
	 		callback("Invalid date cutoff for search.");
	 	}
	},

	/*	Get the average percentage of students who have eaten the top N snacks within a specified timeframe, in a given class. Ordered by number of times
		each snack was administered. If no classroom or class type given, use all classes.
		Callback on error and snacks array with name and average percentage who eat it */
	getSnackStats: function(dateCutoff, classroomUID, classTypeUID, numSnacks, callback) {
		// ensure date cutoff exists and is valid moment object
		if (dateCutoff && dateCutoff.isValid()) {
			// query for selecting reportUID, did eat, and snack name from individual records in this timeframe (child only)
			var query = 'SELECT i.reportUID, i.didEat, s.name AS snack FROM individualRecords i JOIN attendanceReports a ON i.reportUID = a.uid LEFT JOIN snacks s ON a.snackUID = s.uid LEFT JOIN classTypes t ON a.classTypeUID = t.uid WHERE a.whenRecorded > ? AND t.isAdult = 0 AND s.uid != 1';
			var params = [dateCutoff.format('YYYY-MM-DD')];
			
			// if classroom constraint given
			if (classroomUID) {
				// add condition to query and update params
				query += ' AND a.classroomUID = ?';
				params.push(classroomUID);
			}

			// if class type constraint given
			if (classTypeUID) {
				// add condition to query and update params
				query += ' AND a.classTypeUID = ?';
				params.push(classTypeUID);
			}

			query += ';';	// add semicolon to end of query

			// select all individual records in the given timeframe
			con.query(query, params, function(err, rows) {
				if (!err && rows !== undefined) {
					// association of report UID to array of all records in that report
					var reports = {};

					// for each individual record
					for (var i = 0; i < rows.length; i++) {
						// add record to the corresponding array of records grouped by report
						if (!reports[rows[i].reportUID]) {
							reports[rows[i].reportUID] = [rows[i]];
						} else {
							reports[rows[i].reportUID].push(rows[i]);
						}
					}

					// association of each recorded snack with its stats
					var snacks = {};

					// for each report
					for (var id in reports) {
						if (reports.hasOwnProperty(id)) {
							var records = reports[id];
							
							// ensure record exist before calculating percentage eaten
							if (records.length > 0) {
								var snackName = records[0].snack;
								var percentageEaten = 0;

								// for each record in this report
								for (var i = 0; i < records.length; i++) {
									// if student ate, increment number who ate
									if (records[i].didEat) {
										percentageEaten++;
									}
								}

								// divide by class size to get percentage who ate and multiply by 100 to get percent
								percentageEaten = percentageEaten / records.length * 100;

								// if no existing stats for this snack
								if (!snacks[snackName]) {
									// construct object with snack name, percent eaten, and number of reports that contributed
									snacks[snackName] = {
										name: snackName,
										avgPercentEaten: percentageEaten,
										numReports: 1
									}

								// update the existing stats for this snack
								} else {
									snacks[snackName].avgPercentEaten += percentageEaten;
									snacks[snackName].numReports++;
								}
							}
						}
					}

					var results = [];

					// for each snack stat object
					for (var name in snacks) {
						if (snacks.hasOwnProperty(name)) {
							var stats = snacks[name];

							// divide by number of reports to get average percent eaten
							stats.avgPercentEaten /= stats.numReports;

							// add object to array
							results.push(stats);
						}
					}

					// ensure results exist before sorting
					if (results.length > 0) {
						// sort by number of times snack was given, greatest to least
						results.sort(function(a, b) {
							return b.numReports - a.numReports;
						});

						// take first N items of results array
						results = results.slice(0, numSnacks);

						// round each float to 2 decimal places
						for (var i = 0; i < results.length; i++) {
							results[i].avgPercentEaten = results[i].avgPercentEaten.toFixed(2);
						}
					}

					// callback on top N snack results
					callback(err, results);
				} else {
					// callback on error
					callback(err || "An error occurred finding individual attendance records for calculating snack consumption.");
				}
			});
		} else {
			// callback on error with date cutoff given
			callback("Invalid date cutoff.");
		}
	},

	/*	Get the top N students who have the highest average disciplinary points, over a specified timeframe, in a given class. If no classroom or
		class type given, use all classes.
		Callback on error and students array with uid, name and mean disciplinary points */
	getMeanDisciplinaryPoints: function(dateCutoff, classroomUID, classTypeUID, numStudents, callback) {
		// ensure date given is defined and valid moment object
		if (dateCutoff && dateCutoff.isValid()) {
			// query for selecting individual records in this timeframe (child only)
			var query = 'SELECT s.name AS student, i.studentUID, i.disciplinePoints FROM individualRecords i JOIN attendanceReports a ON i.reportUID = a.uid LEFT JOIN students s ON i.studentUID = s.uid LEFT JOIN classTypes t ON a.classTypeUID = t.uid WHERE a.whenRecorded > ? AND t.isAdult = 0';
			var params = [dateCutoff.format('YYYY-MM-DD')];
			
			// if classroom constraint given
			if (classroomUID) {
				// add condition to query and update params
				query += ' AND a.classroomUID = ?';
				params.push(classroomUID);
			}

			// if class type constraint given
			if (classTypeUID) {
				// add condition to query and update params
				query += ' AND a.classTypeUID = ?';
				params.push(classTypeUID);
			}

			query += ';';	// add semicolon to end of query

			// run query to get records
			con.query(query, params, function(err, rows) {
				if (!err && rows !== undefined) {
					// association from student UID's to student info objects
					var students = {};

					// for each individual record
					for (var i = 0; i < rows.length; i++) {
						if (!students[rows[i].studentUID]) {
							// create new student info object
							students[rows[i].studentUID] = {
								uid: rows[i].studentUID,
								name: rows[i].student,
								meanDisciplinaryPoints: rows[i].disciplinePoints,
								classesAttended: 1
							};
						} else {
							// add to discipline point total & increment number of classes attended
							students[rows[i].studentUID].meanDisciplinaryPoints += rows[i].disciplinePoints;
							students[rows[i].studentUID].classesAttended++;
						}
					}

					var results = [];

					// for each constructed student object
					for (var uid in students) {
						if (students.hasOwnProperty(uid)) {
							var s = students[uid];

							// divide disciplinary total by number of classes to get average
							s.meanDisciplinaryPoints /= s.classesAttended;

							// add student object to results array
							results.push(s);
						}
					}

					if (results.length > 0) {
						// sort student objects by mean disciplinary points
						results.sort(function(a, b) {
							return b.meanDisciplinaryPoints - a.meanDisciplinaryPoints;
						});

						// trim decimals to 2 digits
						for (var i = 0; i < results.length; i++) {
							results[i].meanDisciplinaryPoints = results[i].meanDisciplinaryPoints.toFixed(2);
						}
					}

					
					// callback on top N of ordered students array
					callback(err, results.slice(0, numStudents));

				} else {
					// callback on error
					callback(err || "An error occurred finding individual attendance records for mean disciplinary point calculation.");
				}
			});
		} else {
			// callback on error with date cutoff given
			callback("Invalid date cutoff");
		}
	},

	/*	Gets the top N students who have been absent for M days or more for a specific class (ordered by last date attended, recent to late). 
		If no classroom or class type given, use all classes.
		Callback on error and students array with uid, name, and date of last attendance */
	getAbsentStudents: function(classroomUID, classTypeUID, numStudents, numDaysAbsent, callback) {
		// ensure number of days absent parameter is positive
		if (numDaysAbsent > 0) {
			// query for getting the past N class days
			var getClassDaysQuery = 'SELECT DATE_FORMAT(whenRecorded, "%Y-%m-%d") AS date FROM attendanceReports WHERE 1=1 ';
			var getClassDaysParams = [];

			// query for getting the date of last attendance of every student
			var getLastAttendancesQuery = 'SELECT DATE_FORMAT(MAX(a.whenRecorded), "%Y-%m-%d") AS date, i.studentUID, s.name AS student FROM individualRecords i JOIN attendanceReports a ON i.reportUID = a.uid JOIN students s ON i.studentUID = s.uid WHERE 1=1 ';
			var getLastAttendancesParams = [];

			// if classroom constraint given
			if (classroomUID) {
				// add condition to both queries and update params
				getClassDaysQuery += 'AND classroomUID = ? ';
				getClassDaysParams.push(classroomUID);

				getLastAttendancesQuery += 'AND a.classroomUID = ? ';
				getLastAttendancesParams.push(classroomUID);
			}

			// if class type constraint given
			if (classTypeUID) {
				// add condition to query and update params
				getClassDaysQuery += 'AND classTypeUID = ? ';
				getClassDaysParams.push(classTypeUID);

				getLastAttendancesQuery += 'AND a.classTypeUID = ? ';
				getLastAttendancesParams.push(classTypeUID);
			}

			// finish first query and add number of days absent parameter
			getClassDaysQuery += 'GROUP BY date ORDER BY date DESC LIMIT ?;'
			getClassDaysParams.push(numDaysAbsent);

			// finish second query
			getLastAttendancesQuery += 'GROUP BY i.studentUID ORDER BY date DESC;'

			// run query to get past N class days
			con.query(getClassDaysQuery, getClassDaysParams, function(err, rows) {
				if (!err && rows !== undefined) {
					// error for requesting a number of days absent that is greater than the number of classes that have occurred
					if (rows.length < numDaysAbsent) {
						// callback on empty array of absentees
						callback(err, []);
					} else {
						// get the class date of N classes ago
						var nthClassDate = moment(rows[rows.length - 1].date);

						// ensure moment parsed date successfully
						if (nthClassDate.isValid()) {
							// run query to get date of last attendance in this class for every student
							con.query(getLastAttendancesQuery, getLastAttendancesParams, function(err, rows) {
								if (!err && rows !== undefined) {
									var lastAttendance;
									var absentStudents = [];

									// for each student who has attended
									for (var i = 0; i < rows.length; i++) {
										// parse last date attended with moment
										lastAttendance = moment(rows[i].date);

										// if parsing successful and their last attendance was at least N days ago
										if (lastAttendance.isValid() && lastAttendance.isBefore(nthClassDate)) {
											// add object to array of absent students
											absentStudents.push({
												uid: rows[i].studentUID,
												name: rows[i].student,
												dateLastAttended: lastAttendance.format('MMMM Do, YYYY')
											});
										}
									}

									// callback on top N students who have been absent M days or more
									callback(err, absentStudents.slice(0, numStudents));
								} else {
									// error on no students having attended this class
									callback(err || "No records of attendance found for this class in the given timeframe.");
								}
							});
						} else {
							// error on date invalidity
							callback("Invalid class date from " + numDaysAbsent + " class days ago.");
						}
					}
				} else {
					// error for no class days found
					callback(err || "No reports of class found.");
				}
			});
		} else {
			// error for invalid numDaysAbsent parameter
			callback("Number of days absent must be a positive integer.");
		}
	},

	/*	Condenses chart data if there are too many points
		Return array with chart data */	
	condenseChartData(data) {
		// if data has too many points
		if (data.length > sys.maxNumChartPoints) {
			// calculate interval at which data will be averaged
			var interval = Math.ceil(data.length / sys.maxNumChartPoints);

			// array for new, condensed data
			var condensed = [];

			// for each group
			for (var i = 0; i < data.length; i += interval) {
				var avg = 0, numPoints = 0;

				// for each data point in group
				for (var j = 0; j < interval && i + j < data.length; j++) {
					avg += data[i + j].y;	// add to sum of points
					numPoints++;		// increment number of points contributing to average
				}

				// divide sum by number of points (or 1 to avoid div by 0)
				avg /= numPoints == 0 ? 1 : numPoints;
				
				// add new data point with averaged y-value
				condensed.push({ x: data[i].x, y: avg.toFixed(2) });
			}

			// return condensed dataset
			return condensed;
	
		// no change to data
		} else {
			return data;
		}
	}

}

/* 	Determine information about the classroom and class type given
	Callback on error, all classes used?, classroom name, classtype name */
function getMetaClassInfo(classroomUID, classTypeUID, callback) {
	if (!(classroomUID || classTypeUID)) {
		// callback on all classes
		callback(null, true);
	} else {
		// get classroom, time and level info from db
		con.query('SELECT * FROM classrooms WHERE uid = ?; SELECT * FROM classTypes WHERE uid = ?;', [classroomUID, classTypeUID], function(err, rows) {
			if (!err && rows !== undefined && rows.length > 1) {
				// extract names & isAdult from query result
				var classroom = rows[0].length > 0 ? rows[0][0].name : null;
				var classType = rows[1].length > 0 ? rows[1][0].name : null;
				var isAdult = rows[1].length > 0 ? rows[1][0].isAdult == 1 : false;

				// if neither is defined still
				if (!(classroom || classType)) {
					// callback on error that records were not found with these UIDs
					callback("Unable to find class data.");
				} else {
					// callback on allClasses: false, classroom, and class type
					callback(err, false, classroom, classType, isAdult);
				}
			} else {
				callback(err || "Unable to find classroom or class type data.");
			}
		});
	}
}