
var con = require('./database.js').connection;		// connection to DB for running queries
var moment = require('moment');						// moment for date operations
var casual = require('casual');						// lib for generating random text

const NUM_MIDDLE_NAMES = 2;			// average number of middle names
const NUM_LAST_NAMES = 2;				// average number of last names
const PROB_ADULT = 0.25;				// percentage of students who are adult
const PROB_CIRCUMSTANCES = 0.15;		// probability student profile has special circumstances
const PROB_ADDRESS = 0.4; 			// probability student profile has address
const PROB_PHONE = 0.25;				// probability student profile has phone
const PROB_UNIFORM = 0.5; 			// probability student profile has uniform
const PROB_ISADMIN = 0.4;				// probability a system user is an admin
const PROB_ISARCHIVED = 0.2;		// probability a system user has been archived
const AVG_CLASS_SIZE = 17;			// average class size
const CLASS_WIGGLE = 5;				// wiggle room on class sizes (variance)
const PROB_ADULT_CLASS = 0.3;			// probability a class is adult class
const PROB_REPORT_COMMENTS = 0.6;		// probability a report has comments
const PROB_ATE = 0.7;					// probability a student ate
const MAX_DISC_POINTS = 3;			// maximum number of disciplinary points possible
const PROB_STU_COMMENTS = 0.2; 		// probability a student gets individual comments
const AVG_NUM_TEACHERS = 2;			// average number of teacher associated with a given report
const PROB_CLASS_OCCURS = 0.9;		// probability that, on a CLASS DAY, class does occur

const studentsToGenerate = 350;		// number of fake student profiles to generate
const usersToGenerate = 20;			// number of fake user profiles (admin & teacher) to generate

// dates betwixt which to generate fake reports
var startDate = moment('2018-01-01');
var endDate = moment();

// which days of the week does class occur on
var classDays = [
	"Mo",
	"Tu",
	"We",
	"Th",
	"Fr"
];

// first names for students / parents
var firstNames = ["Sofia","Isabella","Camila","Valentina","Valeria","Mariana","Luciana","Daniela","Gabriela","Victoria","Martina","Lucia","Ximena/Jimena","Sara","Samantha","Maria José","Emma","Catalina","Julieta","Mía","Antonella","Renata","Emilia","Natalia","Zoe","Nicole","Paula","Amanda","María Fernanda","Emily","Antonia","Alejandra","Juana","Andrea","Manuela","Ana Sofia","Guadalupe","Agustina","Elena","María","Bianca","Ariana","Ivanna","Abril","Florencia","Carolina","Maite","Rafaela","Regina","Adriana","Michelle","Alma","Violeta","Salomé","Abigail","Juliana","Valery","Isabel","Montserrat","Allison","Jazmín","Julia","Lola","Luna","Ana","Delfina","Alessandra","Ashley","Olivia","Constanza","Paulina","Rebeca","Carla","María Paula","Micaela","Fabiana","Miranda","Josefina","Laura","Alexa","María Alejandra","Luana","Fátima","Sara Sofía","Isidora","Malena","Romina","Ana Paula","Mariangel","Amelia","Elizabeth","Aitana","Ariadna","María Camila","Irene","Silvana","Clara","Magdalena","Sophie","Josefa","Santiago","Sebastián","Matías","Mateo","Nicolás","Alejandro","Diego","Samuel","Benjamín","Daniel","Joaquín","Lucas","Tomas","Gabriel","Martín","David","Emiliano","Jerónimo","Emmanuel","Agustín","Juan Pablo","Juan José","Andrés","Thiago","Leonardo","Felipe","Ángel","Maximiliano","Christopher","Juan Diego","Adrián","Pablo","Miguel Ángel","Rodrigo","Alexander","Ignacio","Emilio","Dylan","Bruno","Carlos","Vicente","Valentino","Santino","Julián","Juan Sebastián","Aarón","Lautaro","Axel","Fernando","Ian","Christian","Javier","Manuel","Luciano","Francisco","Juan David","Iker","Facundo","Rafael","Alex","Franco","Antonio","Luis","Isaac","Máximo","Pedro","Ricardo","Sergio","Eduardo","Bautista","Miguel","Cristóbal","Kevin","Jorge","Alonso","Anthony","Simón","Juan","Joshua","Diego Alejandro","Juan Manuel","Mario","Alan","Josué","Gael","Hugo","Matthew","Ivan","Damián","Lorenzo","Juan Martín","Esteban","Álvaro","Valentín","Dante","Jacobo","Jesús","Camilo","Juan Esteban","Elías"];

// last names for students / parents
var lastNames = ["Abar","Abila","Acebo","Adriano","Aguero","Aguilar","Alameda","Alamillo","Alanis","Alatorre","Alberto","Alcaraz","Alegria","Alejo","Aleman","Alire","Allende","Alonzo","Alvarez","Alvaro","Amaya","Amor","Andrada","Angeles","Anguiano","Antonio","Aquino","Aranda","Arenas","Armendarez","Armenta","Arroyo","Atienza","Avalos","Avila","Badilla","Bailon","Balcazar","Balderas","Bandera","Barbero","Bardales","Baro","Baylon","Belmonte","Bencomo","Benito","Bernardo","Berra","Botero","Caldera","Calderon","Camarillo","Camero","Candella","Caraballo","Carrera","Casas","Castellano","Castillo","Chavez","Che","Ciervo","Clemente","Contrera","Costales","Cueva","De Avila","De Jesus","De la Pena","De la Rosa","De Toro","Degallato","Del Campo","Delgado","Diaz","Diego","Domingo","Duran","Enrique","Escandon","Escobar","Esperanza","Esteban","Estrada","Exposito","Fanta","Felix","Fernandez","Ferrera","Florentino","Frontera","Fuentes","Gabaldon","Garcia","Gomez","Gonzales","Gonzaga","Gordon","Granada","Guerra","Guiterrez","Hernandez","Hidalgo","Huerta","Jaime","Jaramillo","Javier","Jiminez","Juaquin","Juarez","La Torre","Lago","Lanzo","Leo","Lopez","Lorenzo","Lovato","Lucia","Maduro","Mandes","Mano","Manuel","Marco","Mareno","Martinez","Mathias","Mejia","Mendoza","Montes","Morales","Munos","Murillo","Najarro","Neri","Nunez","Ocanas","Olivarez","Ortega","Ortiz","Pablo","Padilla","Padron","Palmero","Parilla","Pena","Peralez","Perez","Pinto","Prado","Puerta","Ramon","Raya","Real","Reyes","Rivas","Rivera","Rodriguez","Rojas","Romero","Rosas","Rozario","Rubio","Ruiz","Saenz","Sala","Salamanca","Salas","Salazar","Salvador","Salvo","San Miguel","Sanchez","Santana","Santos","Serrano","Sevilla","Silvera","Solano","Sosa","Sotomayor","Suarez","Tadeo","Teodoro","Tineo","Toledo","Torrez","Trevino","Trinidad","Trujillo","Urbano","Urias","Valdez","Valenciano","Vallez","Varas","Vargas","Vasco","Vega","Vela","Velasquez","Vera","Verde","Vidales","Villanova","Villanueva","Villas","Yanez","Zacarias","Zapata"];

// addresses for students
var addresses = ["11 Pine St. ","Ames, IA 50010","8477 Cypress St. ","Fuquay Varina, NC 27526","7254 Rockwell St. ","Odenton, MD 21113","2 Tunnel St. ","Ashtabula, OH 44004","810 York Court ","Barrington, IL 60010","62 Bald Hill Rd. ","Twin Falls, ID 83301","7615 Alderwood Lane ","Pompano Beach, FL 33060","723 Andover Ave. ","Martinsville, VA 24112","95 Circle Ave. ","Basking Ridge, NJ 07920","8298 State St. ","Moncks Corner, SC 29461","9561 Gates Ave. ","Marlton, NJ 08053","88 Nichols Ave. ","Winter Park, FL 32792","290 N. Heather St. ","Quakertown, PA 18951","780 NW. Beach St. ","Port Chester, NY 10573","48 Bohemia Road ","New Kensington, PA 15068","60 Briarwood St. ","Hempstead, NY 11550","959 Newport Ave. ","Stroudsburg, PA 18360","7316 Ketch Harbour Ave. ","Owensboro, KY 42301","71 Blackburn Drive ","Lake Mary, FL 32746","8437 SW. Glen Eagles Ave. ","Morton Grove, IL 60053","26 St Margarets St. ","Athens, GA 30605","77 West Yukon Street ","Framingham, MA 01701","702 Pilgrim Street ","Patchogue, NY 11772","859 West Newport Dr. ","Downingtown, PA 19335","736 Miles Ave. ","Cape Coral, FL 33904","8029 Hilldale Drive ","Snohomish, WA 98290","608 Cambridge Court ","Kalamazoo, MI 49009","7648 Colonial Street ","Frederick, MD 21701","137 West Winchester St. ","Butler, PA 16001","736 South Miller Lane ","North Bergen, NJ 07047","325 Fairground Street ","Forest Hills, NY 11375","343 Devon Street ","Glen Cove, NY 11542","305 Pulaski St. ","Butte, MT 59701","5 E. Highland Ave. ","Cambridge, MA 02138","19 West Birch Hill Street ","Ballston Spa, NY 12020","9465 High Noon Street ","Ocoee, FL 34761","7 Rocky River Drive ","Stillwater, MN 55082","326 Hillside Rd. ","Calhoun, GA 30701","557 4th Avenue ","Ashland, OH 44805","29 Glenholme Street ","Parkersburg, WV 26101","465 Rocky River Lane ","Reisterstown, MD 21136","643 Gartner Rd. ","Decatur, GA 30030","25 N. Fulton Road ","Orange, NJ 07050","7552 Beech St. ","Palatine, IL 60067","93 Shady St. ","Indian Trail, NC 28079","7722 West Roehampton Street ","Valdosta, GA 31601","444 Tower Street ","San Diego, CA 92111","853 Wentworth St. ","Ponte Vedra Beach, FL 32082","91 Cedar Circle ","Hudson, NH 03051","94 North High Point Ave. ","Lorain, OH 44052","4 Brickyard Ave. ","Lindenhurst, NY 11757","19 Anderson Street ","Havertown, PA 19083","8621 SE. Lafayette Dr. ","Clermont, FL 34711","759 Summerhouse Street ","Horn Lake, MS 38637","898 Columbia Drive ","Calumet City, IL 60409","602 Heritage Circle ","Princeton, NJ 08540","7934 Rockledge Ave. ","Hollywood, FL 33020","177 Greenrose Street ","Wheaton, IL 60187","7935 Laurel Lane ","El Dorado, AR 71730","753 South Young Court ","Rolla, MO 65401","787 Cooper Ave. ","Antioch, TN 37013","8481 Glenlake Court ","West Fargo, ND 58078","54 Big Rock Cove Ave. ","Union City, NJ 07087","9818 Helen Ave. ","Wyandotte, MI 48192","92 Brown Road ","Menasha, WI 54952","784 Oak St. ","Glenview, IL 60025","381 Locust Dr. ","Mountain View, CA 94043","7267 W. Leeton Ridge Drive ","Catonsville, MD 21228","9 Bedford St. ","Loxahatchee, FL 33470","85 NW. Surrey St. ","Conyers, GA 30012","62 W. Theatre St. ","Nanuet, NY 10954","8513 S. Westminster Ave. ","Saint Paul, MN 55104","8099 Mill Ave. ","Ocean Springs, MS 39564","1 Pawnee Ave. ","Burnsville, MN 55337","83 Prospect Street ","Lacey, WA 98503","7489 North Spring Lane ","Merrillville, IN 46410","9108 Cooper Ave. ","Oak Forest, IL 60452","8 Carpenter Drive ","Grand Haven, MI 49417","640 White St. ","Montgomery Village, MD 20886","69 East Lake Ave. ","Wausau, WI 54401","5 Coffee Court ","Sidney, OH 45365","34 St Louis St. ","North Tonawanda, NY 14120","270 Gates Lane ","Leominster, MA 01453","9697 Beech Ave. ","Old Bridge, NJ 08857","579 Southampton Street ","Garfield, NJ 07026","909 Bow Ridge Lane ","Hummelstown, PA 17036","8693 Clinton Rd. ","Independence, KY 41051","9800 Cambridge Road ","New Bedford, MA 02740","7878 SW. Pineknoll Court ","Hightstown, NJ 08520","9226 Summit St. ","Greenfield, IN 46140","63 Johnson Lane ","Gaithersburg, MD 20877","345 E. Fawn Street ","Southgate, MI 48195","7164 Bridge Dr. ","Waterford, MI 48329","3 Birchwood Dr. ","Newton, NJ 07860","6 Carson Street ","Elkridge, MD 21075","38 Valley Rd. ","Osseo, MN 55311","21 Pawnee Rd. ","Joliet, IL 60435","873 William Street ","Hanover, PA 17331","42 Glen Ridge Ave. ","Greensboro, NC 27405","733 Arch St. ","Apple Valley, CA 92307","9464 Cottage Ave. ","Evans, GA 30809","53 E. Windfall Ave. ","Hixson, TN 37343","7834 W. Goldfield Drive ","Roselle, IL 60172","438 Rockville St. ","East Brunswick, NJ 08816","62 Primrose Street ","Oak Park, MI 48237","645 Clay St. ","Chelsea, MA 02150","985 Temple Drive ","Macungie, PA 18062","409 High Noon Ave. ","Andover, MA 01810","74 NW. Kingston Ave. ","Smithtown, NY 11787","765 Depot Street ","New City, NY 10956","7561 Hudson Drive ","Mchenry, IL 60050","398 Valley Farms Dr. ","Shakopee, MN 55379","3 School Dr. ","Coventry, RI 02816","401 Ridgeview St. ","Neenah, WI 54956","288 Fieldstone Lane ","South Lyon, MI 48178","7717 Ohio Street ","Woonsocket, RI 02895","216 Lawrence Ave. ","Seattle, WA 98144","172 Princess St. ","Norwalk, CT 06851","9477 Randall Mill Court ","Chesterfield, VA 23832","26 Victoria Avenue ","Downers Grove, IL 60515","558 S. Parker St. ","Fort Mill, SC 29708","9469 Mammoth Ave. ","Shelbyville, TN 37160","35 Lyme Lane ","Richmond, VA 23223","42 N. Oakwood Ave. ","Cranford, NJ 07016","272 West Charles St. ","Carol Stream, IL 60188","69 Jennings Lane ","Riverdale, GA 30274","894 E. Fairway Drive ","Oak Ridge, TN 37830","407 Cemetery Lane ","Littleton, CO 80123","9291 Bellevue Road ","Passaic, NJ 07055","675 Gregory Court ","Enfield, CT 06082","8 Lancaster Street ","Mableton, GA 30126","27 Pine Dr. ","Oxon Hill, MD 20745","9298 Plymouth Ave. ","Springfield Gardens, NY 11413","76 Windsor St. ","Menomonee Falls, WI 53051","12 Smith Store Ave. ","Mankato, MN 56001","8666 Piper Ave. ","Astoria, NY 11102","86 Proctor Street ","Ephrata, PA 17522","411 Beaver Ridge Street ","Cocoa, FL 32927","771 Parker Dr. ","Oviedo, FL 32765","38 South Rockwell St. ","Gwynn Oak, MD 21207","7 E. Fordham Lane ","East Stroudsburg, PA 18301","797 Cedar St. ","Dedham, MA 02026","327 Miller Court ","Zion, IL 60099","8278 West High Ridge St. ","Geneva, IL 60134","981 High Noon Street ","Port Washington, NY 11050","222 Rockville Ave. ","Freeport, NY 11520","8411 Rock Maple St. ","Greer, SC 29650","703 Clay Street ","Glen Allen, VA 23059","7349 Chestnut St. ","Anchorage, AK 99504","8711 Augusta St. ","Burlington, MA 01803","80 Trout St. ","Hopewell Junction, NY 12533","566 Bradford St. ","Augusta, GA 30906","10 Brook St. ","Tacoma, WA 98444","7890 Hudson St. ","Superior, WI 54880","421 Devon Dr. ","Norfolk, VA 23503","302 Shore St. ","Yuba City, CA 95993","38 Longbranch St. ","Middle River, MD 21220","85 North Essex Ave. ","Warner Robins, GA 31088","84 Talbot Street ","San Angelo, TX 76901","8981 W. Vermont St. ","Thornton, CO 80241","24 Hudson Lane ","Halethorpe, MD 21227","795 Bank Street ","Williamstown, NJ 08094","9069 Temple Dr. ","Lake Zurich, IL 60047","3 Atlantic Street ","Houston, TX 77016","542 Miller St. ","Missoula, MT 59801","9730 Woodside St. ","Wilmington, MA 01887","316 Leeton Ridge Road ","Waukegan, IL 60085","491 Trout Lane ","Cherry Hill, NJ 08003","9901 Henry Smith Rd. ","Natick, MA 01760","9817 Lakeview Lane ","Lawrence, MA 01841","18 Orange Ave. ","Owatonna, MN 55060","658 Riverview Ave. ","Arlington, MA 02474","296 College Ave. ","Franklin, MA 02038","7002 N. Vine Lane ","Macomb, MI 48042","7200 Tailwater Drive ","Milton, MA 02186","7 Pheasant St. ","Huntley, IL 60142","641 Linda Rd. ","Clifton, NJ 07011","9 8th St. ","Newtown, PA 18940","406 Lakeshore Ave. ","Langhorne, PA 19047","51 Brandywine Rd. ","East Elmhurst, NY 11369","90 Charles St. ","Revere, MA 02151","214 St Paul Court ","Harrisonburg, VA 22801","485 Glenlake Street ","Wheeling, WV 26003","8791 Pennsylvania Street ","Sykesville, MD 21784","9355 Newbridge Street ","Mechanicsburg, PA 17050","33 North Addison Lane ","Farmington, MI 48331","24 Helen Ave. ","Roseville, MI 48066","284 Rock Maple Ave. ","La Crosse, WI 54601","91 Prince St. ","Mechanicsville, VA 23111","293 Rose Ave. ","Hicksville, NY 11801","5 Pendergast Street ","Hartsville, SC 29550","46 E. Hanover Ave. ","Brookfield, WI 53045","87 Pawnee Circle ","Bedford, OH 44146","144 North Stillwater St. ","Powder Springs, GA 30127","4 Sunnyslope Ave. ","Hermitage, TN 37076","675 Washington Street ","Hackensack, NJ 07601","907 Tarkiln Hill Ave. ","Piqua, OH 45356","9 N. Trenton Dr. ","Daphne, AL 36526","8301 East Orange Ave. ","Dunedin, FL 34698","58 8th Street ","New Britain, CT 06051","95 Columbia Rd. ","Arvada, CO 80003","407 Fairway Road ","Waynesboro, PA 17268","71 Magnolia St. ","Hope Mills, NC 28348","244 Tanglewood Lane ","Elizabethton, TN 37643","8381C Squaw Creek St. ","Norwood, MA 02062","88 2nd Ave. ","Newark, NJ 07103","8225 Carpenter Street ","Atlantic City, NJ 08401","16 Harvey Lane ","Middle Village, NY 11379","46 South Heather St. ","Mount Airy, MD 21771","8790 Bowman Street ","Savannah, GA 31404","17 Lafayette St. ","Rego Park, NY 11374","796 Brickell Court ","Valparaiso, IN 46383","8899 Howard Rd. ","Dundalk, MD 21222","7180 Cleveland Drive ","Fitchburg, MA 01420","42 SE. Chestnut Lane ","Hyattsville, MD 20782","47 Amerige Court ","Philadelphia, PA 19111","872 Fairground Circle ","Wethersfield, CT 06109","8525 Sunbeam Dr. ","Quincy, MA 02169","6 NE. Littleton Court ","Hickory, NC 28601","8395 Woodsman Rd. ","Mount Prospect, IL 60056","473 Del Monte St. ","Melrose, MA 02176","656 East High Ridge St. ","Louisville, KY 40207","81 Hilldale Court ","Irmo, SC 29063","6 West Bellevue Ave. ","Saint Petersburg, FL 33702","9162 Green Hill St. ","Lake Jackson, TX 77566","8379 Vernon Street ","Clarksburg, WV 26301","578 Wayne Street ","Ada, OK 74820","573 South Thomas Ave. ","Elizabeth, NJ 07202","649 Mayflower Circle ","Mesa, AZ 85203","7137 Beacon Ave. ","Grand Forks, ND 58201","33 Cardinal St. ","Beltsville, MD 20705","847 Heritage Circle ","West Hempstead, NY 11552","690 Rosewood St. ","Williamsburg, VA 23185","9320 Summer Drive ","Canal Winchester, OH 43110","9148 Woodland Street ","Kokomo, IN 46901","7343 Roosevelt Street ","Cheshire, CT 06410","8953 W. Henry St. ","Taunton, MA 02780","9297 Newport Rd. ","Ottumwa, IA 52501","44 Mammoth St. ","Simpsonville, SC 29680","8337 Temple Court ","Biloxi, MS 39532","454 Creek St. ","Boca Raton, FL 33428","7161 Mulberry Ave. ","Birmingham, AL 35209","8815 4th Ave. ","Eastpointe, MI 48021","4 6th Lane ","Pelham, AL 35124","232 53rd Street ","Eastlake, OH 44095","399 Ramblewood Ave. ","Ft Mitchell, KY 41017","6 East Plumb Branch Drive ","Colorado Springs, CO 80911","336 Oak Ave. ","Trussville, AL 35173","871 Newcastle Ave. ","Hattiesburg, MS 39401","358 Marsh St. ","Marlborough, MA 01752"];

// user phone numbers
var phones = ["(617) 365-4320","(555) 909-7903","(771) 954-7468","(744) 536-9713","(688) 929-5821","(818) 760-2291","(812) 488-1392","(302) 616-5562","(509) 327-4531","(252) 625-1309","(893) 887-0067","(819) 944-3052","(967) 757-5142","(860) 605-1941","(657) 725-8013","(506) 848-2162","(272) 711-1075","(952) 715-2870","(466) 261-4782","(413) 868-7377","(285) 237-4829","(308) 255-9104","(497) 725-0586","(444) 732-0869","(907) 848-3290","(685) 563-6159","(708) 949-5229","(816) 377-9253","(362) 995-3977","(601) 871-1601","(271) 520-9216","(313) 965-7816","(771) 332-0818","(450) 653-1680","(936) 215-6008","(361) 920-3520","(642) 296-4706","(700) 586-9336","(972) 478-1120","(620) 514-6363","(360) 436-7170","(427) 206-4505","(836) 985-7701","(943) 439-6055","(722) 900-2201","(667) 630-9552","(752) 229-8554","(544) 338-4791","(766) 409-9215","(849) 541-1561","(415) 533-2636","(668) 636-8419","(974) 430-1151","(494) 547-1542","(200) 710-9222","(672) 262-2917","(214) 750-9696","(490) 570-0470","(819) 780-9516","(934) 652-0722","(670) 795-5394","(485) 312-8584","(811) 652-7789","(663) 347-1612","(346) 841-9125","(967) 297-7134","(511) 619-0693","(952) 541-8860","(426) 689-4587","(680) 953-5220","(361) 860-9007","(251) 264-1150","(990) 487-9183","(527) 618-7347","(455) 266-7418","(719) 693-6396","(451) 922-2183","(490) 327-7447","(535) 780-1926","(688) 230-9693","(647) 711-7382","(693) 384-8296","(898) 905-0560","(494) 829-1954","(981) 927-3526","(647) 458-5231","(824) 406-7135","(598) 640-3443","(867) 613-2896","(631) 745-9798","(779) 919-0436","(940) 232-0372","(525) 961-0695","(719) 757-5520","(409) 566-0947","(825) 696-4093","(298) 791-3279","(796) 698-9955","(856) 656-5858","(566) 715-9560","(918) 607-6979","(512) 479-0076","(544) 710-5334","(320) 670-7490","(847) 860-5629","(431) 616-8363","(605) 978-1252","(986) 900-4298","(393) 320-1775","(487) 852-2010","(246) 226-7881","(931) 202-4024","(619) 854-3320","(424) 614-8967","(684) 818-4574","(829) 837-0320","(581) 414-4582","(633) 777-0788","(983) 721-5489","(219) 541-7058","(979) 292-3739","(421) 367-9869","(511) 450-7958","(309) 494-7086","(976) 900-5831","(220) 277-7482","(439) 902-9722","(716) 777-7004","(441) 360-6328","(563) 657-3119","(939) 245-1498","(947) 293-5939","(264) 315-1118","(438) 509-8153","(989) 776-8735","(764) 803-0878","(724) 377-8184","(533) 398-6024","(340) 258-3953","(675) 788-0961","(241) 334-8086","(790) 862-4614","(332) 558-2652","(624) 991-8652","(913) 535-5345","(848) 886-2163","(230) 581-8744","(512) 492-0214","(392) 735-7788","(330) 378-4775","(891) 674-3772","(482) 822-5206","(857) 639-4506","(401) 284-8154","(633) 296-3323","(446) 896-0242","(893) 262-3352","(997) 673-3218","(657) 890-1181","(468) 981-8925","(279) 529-3196","(881) 744-5530","(877) 832-0832","(342) 504-5456","(821) 496-6155","(645) 992-5563","(498) 993-1464","(813) 217-0486","(888) 311-2871","(644) 800-7617","(431) 585-5109","(804) 939-3642","(554) 727-1642","(674) 850-4979","(526) 336-2651","(532) 422-0868","(332) 497-4972","(530) 284-0425","(283) 445-8644","(618) 487-7910","(383) 921-3367","(229) 553-6543","(590) 695-7427","(762) 777-1310","(467) 476-4265","(969) 605-8629","(781) 722-7867","(918) 223-2547","(223) 417-6849","(643) 252-2220","(978) 473-5896","(910) 859-8666","(481) 777-1978","(929) 697-4666","(921) 729-5112","(306) 570-4612","(618) 426-7744","(363) 853-3754","(722) 365-0438","(864) 639-3268","(652) 393-7308","(570) 611-2667","(677) 755-5445","(448) 742-8515","(729) 223-7612","(518) 840-3196","(506) 302-6152","(328) 414-7262","(682) 496-3769","(625) 652-0100","(778) 411-5304","(713) 456-1741","(988) 367-7225","(502) 410-3333","(862) 356-3318","(765) 447-4675","(204) 644-6395","(917) 782-3256","(400) 488-7028","(313) 633-9487","(828) 627-7824","(866) 741-5602","(767) 451-0224","(921) 249-4011","(305) 405-0043","(914) 338-2721","(898) 299-3215","(249) 597-0771","(658) 349-4878","(237) 383-2365","(816) 542-9199","(716) 518-7113","(579) 221-1079","(903) 603-7646","(736) 929-8532","(890) 994-6111","(580) 705-6928","(551) 841-3271","(945) 208-4759","(890) 520-5831","(769) 831-4895","(774) 769-0353","(994) 557-5253","(580) 201-7079","(568) 486-6011","(488) 686-6820","(555) 370-7322","(898) 929-8266","(523) 500-6055","(776) 659-9777","(811) 673-2181","(860) 913-3606","(948) 482-5952","(895) 363-4466","(335) 730-8648","(447) 799-4061","(781) 390-5720","(938) 211-4678","(959) 702-7803","(821) 770-2174","(484) 472-3055","(287) 868-4823","(976) 979-9189","(880) 747-7990","(631) 969-4639","(753) 963-5739","(744) 431-2412","(345) 717-5362","(543) 354-3718","(888) 274-2309","(603) 825-1626","(442) 799-3216","(660) 341-4801","(963) 697-4365","(728) 909-9050","(893) 815-8441","(881) 316-9749","(668) 449-4098","(531) 940-9007","(612) 814-2536","(575) 666-0550","(371) 536-7148","(358) 287-1254","(444) 817-7052","(739) 551-8546","(575) 335-4245","(813) 409-0044","(273) 524-2946","(997) 993-7720","(580) 854-1199","(974) 511-8470","(612) 286-5878","(957) 296-7559","(710) 771-1942","(503) 624-2157","(829) 427-3110","(613) 201-5764","(462) 650-2514","(574) 632-4963","(929) 857-7237"];

// emails for user accounts (admins & teachers)
var userEmails = ["tristan@gmail.com","iamcal@gmail.com","lahvak@gmail.com","larry@gmail.com","jemarch@gmail.com","gommix@gmail.com","salesgeek@gmail.com","library@gmail.com","openldap@gmail.com","bwcarty@gmail.com","matsn@gmail.com","gordonjcp@gmail.com","lipeng@gmail.com","wkrebs@gmail.com","wildfire@gmail.com","jamuir@gmail.com","munjal@gmail.com","birddog@gmail.com","zavadsky@gmail.com","ateniese@gmail.com","geeber@gmail.com","jbailie@gmail.com","dhrakar@gmail.com","bulletin@gmail.com","barlow@gmail.com","marin@gmail.com","wikinerd@gmail.com","caronni@gmail.com","mkearl@gmail.com","adhere@gmail.com","kobayasi@gmail.com","mcast@gmail.com","josephw@gmail.com","mhoffman@gmail.com","kjetilk@gmail.com","janusfury@gmail.com","manuals@gmail.com","gumpish@gmail.com","lstaf@gmail.com","maradine@gmail.com","drolsky@gmail.com","aegreene@gmail.com","chlim@gmail.com","dcoppit@gmail.com","mrdvt@gmail.com","jkegl@gmail.com","atmarks@gmail.com","murdocj@gmail.com","ilikered@gmail.com","baveja@gmail.com","naoya@gmail.com","pierce@gmail.com","peoplesr@gmail.com","zeller@gmail.com","richard@gmail.com","mschwartz@gmail.com","madler@gmail.com","bowmanbs@gmail.com","sblack@gmail.com","kidehen@gmail.com","tsuruta@gmail.com","scato@gmail.com","munge@gmail.com","seasweb@gmail.com","dbindel@gmail.com","frosal@gmail.com","lstein@gmail.com","augusto@gmail.com","killmenow@gmail.com","sumdumass@gmail.com","aukjan@gmail.com","maneesh@gmail.com","uncle@gmail.com","drjlaw@gmail.com","pjacklam@gmail.com","wainwrig@gmail.com","parsimony@gmail.com","gozer@gmail.com","dinther@gmail.com","kodeman@gmail.com","vlefevre@gmail.com","jimxugle@gmail.com","jginspace@gmail.com","sacraver@gmail.com","kdawson@gmail.com","agapow@gmail.com","iapetus@gmail.com","laird@gmail.com","mfburgo@gmail.com","stewwy@gmail.com","skaufman@gmail.com","tubajon@gmail.com","tkrotchko@gmail.com","pkplex@gmail.com","bartlett@gmail.com","louise@gmail.com","dkeeler@gmail.com","henkp@gmail.com","ismail@gmail.com","portscan@gmail.com","violinhi@gmail.com","esasaki@gmail.com","north@gmail.com","mgemmons@gmail.com","stern@gmail.com","jmcnamara@gmail.com","bonmots@gmail.com","policies@gmail.com","ranasta@gmail.com","ingolfke@gmail.com","heroine@gmail.com","sravani@gmail.com","pfitza@gmail.com","cisugrad@gmail.com","nanop@gmail.com","zeitlin@gmail.com","kassiesa@gmail.com","campbell@gmail.com","horrocks@gmail.com","whimsy@gmail.com","jgmyers@gmail.com","balchen@gmail.com","pavello@gmail.com","josem@gmail.com","jbryan@gmail.com","bartak@gmail.com","bockelboy@gmail.com","esokullu@gmail.com","neuffer@gmail.com","fwiles@gmail.com","frode@gmail.com","isotopian@gmail.com","animats@gmail.com","hutton@gmail.com","sagal@gmail.com","peterhoeg@gmail.com","luvirini@gmail.com","andersbr@gmail.com","markjugg@gmail.com","timlinux@gmail.com","dgriffith@gmail.com","scottzed@gmail.com","bsikdar@gmail.com","amcuri@gmail.com","nichoj@gmail.com","symbolic@gmail.com","thaljef@gmail.com","gravyface@gmail.com","themer@gmail.com","rhialto@gmail.com","luebke@gmail.com","treit@gmail.com","sherzodr@gmail.com","chaikin@gmail.com","durist@gmail.com","jimmichie@gmail.com","csilvers@gmail.com","chance@gmail.com","malvar@gmail.com","phizntrg@gmail.com","konit@gmail.com","tlinden@gmail.com","jwarren@gmail.com","carreras@gmail.com","payned@gmail.com","sinkou@gmail.com","rsteiner@gmail.com","pavel@gmail.com","dimensio@gmail.com","research@gmail.com","burniske@gmail.com","hermanab@gmail.com","punkis@gmail.com","nullchar@gmail.com","hyper@gmail.com","njpayne@gmail.com","chrwin@gmail.com","johnh@gmail.com","mjewell@gmail.com","ninenine@gmail.com","pappp@gmail.com"];

// names for user accounts (admin & teachers)
var userNames = ["Aedan Jones","Alec Salinas","Eli Hall","Malaki Lawrence","Meghan Church","Keagan Anderson","Areli Nunez","Kadyn Hall","Andrea Williams","Yusuf York","Mckinley Khan","Jayden Patton","Tamia Macdonald","Keely Schroeder","Ryder Horn","Abril Fletcher","Mathew Orr","Amirah Hanna","Abel Hanson","Gideon Sutton","Leo Moore","Jovany Potter","Katie Mckinney","Jenny Hamilton","Laila Padilla","Nathaniel Bryant","Avery Bullock","Ishaan Downs","Carly Ford","Lilianna Fisher","Priscilla Irwin","Willie Santiago","Myla Chase","Tianna Shepherd","Emilee Harmon","Maria Norman","Kaylyn Melton","Arabella Singleton","Caleb White","Karson Stein","Aspen Yang","Kayley Beck","Jazmyn Mora","Harper Zavala","Kaley Bolton","Austin Reyes","Kaden Kim","Aliana Sullivan","Janessa Valentine","Trent Malone","Abigail Carrillo","Gwendolyn Ho","Erin Ryan","Lexie Hanson","Cassius Stark","Brennan Ramsey","Isaias Gilmore","Stacy Turner","Kenna Goodman","Ayana Villanueva","Dale Chung","Jaelynn Martin","Bryant Allen","Kaelyn Stephenson","Avery Collins","Jaime Holmes","Celeste Hobbs","Nora Moon","Andrea Nash","Angie Hines","Kallie Benjamin","Brice Gamble","Cierra Murray","Jaiden Hensley","Levi Bolton","Serena Quinn","Gillian Frazier","Royce Choi","Willow Maynard","Ryan Cochran","Octavio Huynh","Jeramiah Logan","Mara Gardner","Ramiro Rhodes","Damion Robbins","Kenny Villanueva","Casey Hamilton","Aryan Meyers","Jaxon Gutierrez","Charlie Odonnell","Tucker Small","Carlee Greer","Cassandra Mcbride","Jaylene Waters","Gia Dodson","Tripp Skinner","Deanna Conway","Lainey Gibson","June Salinas","Kason Powell","Jairo Larsen","Macey Roy","Gideon Jenkins","Ally Robertson","Brody Hess","Leland Solomon","Pierce Booker","Yusuf Walls","Joyce Barrera","Jermaine Warren","Gabrielle Maldonado","Patricia Berg","Anahi Lopez","Diego Willis","Brooke Sanders","Raymond Knapp","Mina Kaufman","Naima Johns","Angelique Romero","Avery Avila","Zaniyah Bradley","Margaret Melendez","Ishaan Cuevas","Janet Mccann","Kelvin Powell","Glenn Rivas","Noel Simmons","Kaiya Morrison","Marlene Holmes","Kamora Ray","Bentley Buckley","Aliya Thomas","Ayla Marks","Chaya Parks","Sara Quinn","Keira Barry","Aditya Curry","Maxwell Sparks","Scarlet Carey","Yoselin Landry","Damarion Ryan","Charles Mosley","Haleigh Fuentes","Frederick Miller","Maverick Norris","Colten Barber","Karley Ritter","Landyn Roy","Armani Oneill","Uriel Heath","Marcel French","Jovan Cline","Triston Rivers","Angelique Carlson","Kolten Ward","Olivia Fuller","Ricky Stanton","Maxwell Blackburn","Mekhi Davila","Jerimiah Mendoza","Alice Mclean","Iris Baxter","Dangelo Greer","Megan Griffith","Jalen David","Caitlin Rowe","Alexander Baird","Wesley Sanchez","Skylar Carlson","Alexus Barton","Sidney Jefferson","Sage Sharp","Chana Deleon","Rory Norton","Deandre Rocha","Donte Ferguson","Ashly Melendez","Jude Oliver","Lily Bennett","Ronald Vazquez","Gloria Archer","Davian Willis","Alondra Gomez","Izabelle Landry","Kevin Garza","Maritza Fuller","Frances York","Evelin Hancock","Theodore Cervantes","Hassan Lowery","Tristen Raymond","Marissa Rivers","Douglas Parker","Leia Hart","Genevieve Butler","Haley Lyons","Amirah Cabrera","Drew James","Sloane Thompson","Case Avery"];

// available uniform sizes
var uniformSizes = [
	"X Small", 
	"Small", 
	"Medium", 
	"Large", 
	"X Large"
];

// snack names
var snacks = [
	"Fruit Salad",
	"Bananas and Peanut Butter Bars",
	"Catrachas",
	"Tomato, Cabbage, and Cheese with Crackers",
	"Vegetables, Cheese, and Crackers",
	"Mini Quiches",
	"Banana Bread",
	"Sliced Papaya",
	"Egg Salad",
	"Corn Salad"
];

// these need to be hard coded in here, since not labeled in DB and there is overlap
var adultClassroomNames = ["PEP Centro", "Volunteer House 1"];
var childClassroomNames = ["PEP Centro", "PEP-2", "PEP-3"];

// add test snacks to DB
function populateSnacksTable(callback) {
	var insert = [];
	for (var i = 0; i < snacks.length; i++) {
		insert.push([snacks[i]]);
	}

	// bulk insert the snacks array
	con.query('INSERT INTO snacks (name) VALUES ?;', [insert], function(err) {
		callback(err);
	});
}

// get random element of an array
function getRand(a) {
	return a[Math.floor(Math.random() * a.length)];
}

// generate a random teacher or admin profile
function getRandomUserProfile() {
	// generate index of random email
	var emailIdx = Math.floor(Math.random() * userEmails.length);

	// start to construct user profile
	var user = {
		name: getRand(userNames),
		email: userEmails[emailIdx],
		isAdmin: Math.random() < PROB_ISADMIN,
		isArchived: Math.random() < PROB_ISARCHIVED
	};

	// remove this email from available
	userEmails.splice(emailIdx, 1);

	var split = user.name.split(" ");

	// get first bit of name
	user.firstName = split[0];

	return user;
}

/* 	Add randomized user accounts to the database 
	Callback on error */
function populateUsersTable(numUsers, callback) {
	if (numUsers < userEmails.length) {
		var insertValues = [];

		// for n students
		for (var i = 0; i < numUsers; i++) {
			// generate a random student profile
			var u = getRandomUserProfile();

			// add values for this student to insert array
			insertValues.push([	u.email, u.name, u.firstName, u.isAdmin, u.isArchived]);
		}

		// insert the student data
		con.query('INSERT INTO users (email, name, firstName, isAdmin, isArchived) VALUES ?;', [insertValues], function(err) {
			callback(err);
		});
	} else {
		callback("Unable to populate users: Not enough emails for the requested amount of users.");
	}
}

/*	Get a random name for a student or parent */
function getStudentOrParentName(givenLastName) {
	// get random first name
	var firstName = getRand(firstNames);

	var middleName = "";

	// randomize number of middle names near avg
	var numMiddleNames = NUM_MIDDLE_NAMES + Math.floor(Math.random() * 2);

	// add all middle names
	for (var i = 0; i < numMiddleNames; i++) {
		// choose random name
		middleName += getRand(lastNames);

		// add hyphen or space between names
		if (i < numMiddleNames - 1) {
			middleName += Math.random() < 0.7 ? "-" : " ";
		}
	}

	var lastName = "";

	// if on last name given, generate a random one
	if (!givenLastName) {
		// randomize number of middle names near avg
		var numLastNames = NUM_LAST_NAMES + Math.floor(Math.random() * 2);

		// add all middle names
		for (var i = 0; i < numLastNames; i++) {
			// choose random name
			lastName += getRand(lastNames);

			// add hyphen or space between names
			if (i < numLastNames - 1) {
				lastName += Math.random() < 0.7 ? "-" : " ";
			}
		}
	} else {
		// set last name to given last name
		lastName = givenLastName;
	}

	// return object with name info
	return {
		fullName: firstName + " " + middleName + " " + lastName,
		first: firstName,
		middle: middleName,
		last: lastName
	};
}

// get a random student profile to insert into DB
function getRandomStudentProfile() {
	// generate random name
	var studentName = getStudentOrParentName();

	// get random parent names with same last name
	var parent1 = getStudentOrParentName(studentName.last);
	var parent2 = getStudentOrParentName(studentName.last);

	var student = {
		name: studentName.fullName,
		parent1: parent1.fullName,
		parent2: parent2.fullName,
		isAdult: Math.random() < PROB_ADULT,
		specialCircumstances: Math.random() < PROB_CIRCUMSTANCES ? casual.sentence : null,
		address: Math.random() < PROB_ADDRESS ? getRand(addresses) : null,
		phone: Math.random() < PROB_PHONE ? getRand(phones) : null,
		uniformSize: Math.random() < PROB_UNIFORM ? getRand(uniformSizes) : null,
	};

	return student;
}

// populate the DB with N test students
function populateTestStudents(numStudents, callback) {
	var insertValues = [];

	// for n students
	for (var i = 0; i < numStudents; i++) {
		// generate a random student profile
		var s = getRandomStudentProfile();

		// add values for this student to insert array
		insertValues.push([s.name, s.parent1, s.parent2, s.isAdult, s.specialCircumstances, s.address, s.phone, s.uniformSize]);
	}

	// insert the student data
	con.query('INSERT INTO students (name, parent1, parent2, isAdult, specialCircumstances, address, phone, uniformSize) VALUES ?;', [insertValues], function(err) {
		callback(err);
	});
}

// https://stackoverflow.com/questions/23795522/how-to-enumerate-dates-between-two-dates-in-moment
var enumerateDaysBetweenDates = function(startDate, endDate) {
    var dates = [];

    var currDate = moment(startDate).startOf('day');
    var lastDate = moment(endDate).startOf('day');

    while(currDate.add(1, 'days').diff(lastDate) < 0) {
        dates.push(currDate.clone());
    }

    return dates;
};

// get boilerplate report objects for each class day within date range
function getReportsOnClassDays(startDate, endDate, adultClassrooms, adultClassTypes, childClassrooms, childClassTypes) {
	var classes = [];

	// extract all adult class UID combos
	for (var i = 0; i < adultClassrooms.length; i++) {
		for (var j = 0; j < adultClassTypes.length; j++) {
			classes.push({ 
				roomUID: adultClassrooms[i].uid, 
				typeUID: adultClassTypes[j].uid,
				adultClass: true 
			});
		}
	}

	// extract all child class UID combos
	for (var i = 0; i < childClassrooms.length; i++) {
		for (var j = 0; j < childClassTypes.length; j++) {
			classes.push({ 
				roomUID: childClassrooms[i].uid, 
				typeUID: childClassTypes[j].uid,
				adultClass: false 
			});
		}
	}

	var dates = enumerateDaysBetweenDates(startDate, endDate);
	var reports = [];

	// for each date in range
	for (var i = 0; i < dates.length; i++) {
		// if class occurs on this day
		if (classDays.includes(dates[i].format('dd'))) {
			// for each possible class
			for (var j = 0; j < classes.length; j++) {
				// if this class occurs that day
				if (Math.random() < PROB_CLASS_OCCURS) {
					// randomize time somewhat within this date
					var whenRecorded = dates[i].clone().set({
						'hour' : 8 + Math.floor(Math.random() * 10),
						'minute': Math.floor(Math.random() * 60),
						'second': Math.floor(Math.random() * 60)
			        });

					// add new boilerplate report to list
					reports.push({
						classroomUID: classes[j].roomUID,
						classTypeUID: classes[j].typeUID,
						adultClass: classes[j].adultClass,
						whenRecorded: whenRecorded.format('YYYY-MM-DD HH:mm:ss')
					});
				}
			}
		}
	}

	return reports;
}

// populate the DB with N attendance reports, generated randomly
function populateAttendanceReports(startDate, endDate, callback) {
	// get needed info to make reports
	con.query('SELECT * FROM classrooms WHERE uid != 1; SELECT * FROM classTypes WHERE isAdult IS NOT NULL; SELECT * FROM snacks WHERE uid != 1; SELECT * FROM users WHERE isAdmin = 0; SELECT * FROM students;', function(err, rows) {
		if (!err && rows !== undefined && rows.length > 0) {
			var classrooms = rows[0];
			var classTypes = rows[1];
			var snacks = rows[2];
			var teachers = rows[3];
			var students = rows[4];

			// extract UIDs of all teachers
			var teacherUIDs = [];
			for (var i = 0; i < teachers.length; i++) {
				teacherUIDs.push(teachers[i].uid);
			}

			// extract separate arrays of UIDs for adult students and child students
			var adultUIDs = [], childUIDs = [];
			for (var i = 0; i < students.length; i++) {
				if (students[i].isAdult) {
					adultUIDs.push(students[i].uid);
				} else {
					childUIDs.push(students[i].uid);
				}
			}

			var adultClassTypes = [], childClassTypes = [];

			// separate into adult and child class types
			for (var i = 0; i < classTypes.length; i++) {
				if (classTypes[i].isAdult) {
					adultClassTypes.push(classTypes[i]);
				} else {
					childClassTypes.push(classTypes[i]);
				}
			}

			var adultClassrooms = [], childClassrooms = [];

			// separate into adult and child classrooms, likewise (THIS IS NOT LABELED IN DB)
			for (var i = 0; i < classrooms.length; i++) {
				// if possible adult location, add
				if (adultClassroomNames.includes(classrooms[i].name)) {
					adultClassrooms.push(classrooms[i]);
				}
				// if possible child location, add
				if (childClassroomNames.includes(classrooms[i].name)) {
					childClassrooms.push(classrooms[i]);
				}
			}

			// association of "<classroomUID>,<classTypeUID>" to list of students who regularly attend that combination
			var regularStudents = {};

			// give each student a regular meeting place
			for (var i = 0; i < students.length; i++) {
				var room, type;

				if (students[i].isAdult) {
					// choose random regular 
					room = getRand(adultClassrooms);
					type = getRand(adultClassTypes);
				} else {
					// choose random regular 
					room = getRand(childClassrooms);
					type = getRand(childClassTypes);
				}

				var combo = room.uid + ',' + type.uid;

				// add student's UID to list of students attending this class regularly
				if (!regularStudents[combo]) {
					regularStudents[combo] = [students[i].uid];
				} else {
					regularStudents[combo].push(students[i].uid);
				}
			}

			// get last report uid
			con.query('SELECT uid FROM attendanceReports ORDER BY uid DESC LIMIT 1;', function(err, rows) {
				if (!err && rows !== undefined) {
					// get uid of last attendance report in db, if any
					var reportUID = rows.length > 0 ? rows[0].uid + 1 : 1;

					var reportInsertValues = [];
					var recordInsertValues = [];
					var teacherReportLinkValues = [];

					// get boilerplate report objects on class days for each possible class combo
					var reports = getReportsOnClassDays(startDate, endDate, adultClassrooms, adultClassTypes, childClassrooms, childClassTypes);

					// for each report
					for (var i = 0; i < reports.length; i++) {
						var report = reports[i];

						// choose random snack if not adult
						report.snackUID = report.adultClass ? null : snacks[Math.floor(Math.random() * snacks.length)].uid;

						// probabilistically add comments
						report.comments = Math.random() < PROB_REPORT_COMMENTS ? casual.sentence : null;

						// add insert values for this report
						reportInsertValues.push([reportUID, report.classroomUID, report.classTypeUID, report.snackUID, report.whenRecorded, report.comments]);

						// determine which students can be put into this class
						var availStudents = regularStudents[report.classroomUID + ',' + report.classTypeUID].slice();

						// calculate a class size with variation
						var classSize = AVG_CLASS_SIZE + (Math.random() < 0.5 ? -1 : 1) * Math.floor(Math.random() * CLASS_WIGGLE);

						// for however many students to generate
						for (var n = 0; n < AVG_CLASS_SIZE; n++) {
							// if students left to add
							if (availStudents.length > 0) {
								// choose random student from available
								var stuIdx = Math.floor(Math.random() * availStudents.length);

								// get UID of student
								var studentUID = availStudents[stuIdx];

								// probabilistically determine did eat
								var didEat = report.adultClass ? null : Math.random() < PROB_ATE;

								// randomize discipline points
								var disciplinePoints = report.adultClass ? null : Math.floor(Math.random() * MAX_DISC_POINTS);

								// probabilistically given comments
								var studentComments = Math.random() < PROB_STU_COMMENTS ? casual.sentence : null;

								// add individual record insert values
								recordInsertValues.push([reportUID, studentUID, didEat, disciplinePoints, studentComments]);

								// remove student UID from available
								availStudents.splice(stuIdx, 1);
							}
						}

						// copy the teacher UID array for choosing/eliminating teachers
						var availTeachers = teacherUIDs.slice();

						// for number of teachers associated with report
						for (var n = 0; n < AVG_NUM_TEACHERS; n++) {
							// choose random student from available
							var teacherIdx = Math.floor(Math.random() * availTeachers.length);

							// get UID of teacher
							var teacherUID = availTeachers[teacherIdx];

							// add individual record insert values
							teacherReportLinkValues.push([reportUID, teacherUID]);

							// remove teacher UID from available
							availTeachers.splice(teacherIdx, 1);
						}

						// move to next report UID
						reportUID++;
					}

					// insert attendance reports
					con.query('INSERT INTO attendanceReports (uid, classroomUID, classTypeUID, snackUID, whenRecorded, comments) VALUES ?;', [reportInsertValues], function(err) {
						if (!err) {
							// insert individual records
							con.query('INSERT INTO individualRecords (reportUID, studentUID, didEat, disciplinePoints, comments) VALUES ?;', [recordInsertValues], function(err) {
								if (!err) {
									// add the teachers to the attendance reports
									con.query('INSERT INTO teacherReportLink (reportUID, teacherUID) VALUES ?;', [teacherReportLinkValues], function(err) {
										callback(err);
									});
								} else {
									callback(err);
								}
							});
						} else {
							callback(err);
						}
					});
				} else {
					callback(err);
				}
			});
		} else {
			callback(err);
		}
	});
}

// populate snacks table
process.stdout.write("Populating snacks table... ");
populateSnacksTable(function(err) {
	if (!err) {
		console.log("Done.");
		// populate users
		process.stdout.write("Populating users table... ");
		populateUsersTable(usersToGenerate, function(err) {
			if (!err) {
				console.log("Done.");

				// populate random student profiles
				process.stdout.write("Populating students table... ");
				populateTestStudents(studentsToGenerate, function(err) {
					if (!err) {
						console.log("Done.");

						// populate attendance reports / individual records / teacher report link tables with random reports
						process.stdout.write("Populating database with attendance reports... ");
						populateAttendanceReports(startDate, endDate, function(err) {
							if (!err) {
								console.log("Done.");
								console.log("Test Suite Complete.");
							} else {
								console.log(err);
							}
						});
					} else {
						console.log(err);
					}
				});
			} else {
				console.log(err);
			}
		});
	} else {
		console.log(err);
	}
});