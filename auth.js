
/* 
	auth.js: Authentication routes / configurations and middleware for restricting pages / requests to various levels of authentication
*/

var GoogleStrategy = require('passport-google-oauth20').Strategy;
var querystring = require('querystring');
var con = require('./database.js').connection;
var creds = require('./credentials.js');

const {OAuth2Client} = require('google-auth-library');
const mobileClient = new OAuth2Client(creds.MOBILE_CLIENT_ID);

// verify an auth token from mobile
async function verify(token, next) {
	try {
		// verify the access token through Google
		const ticket = await mobileClient.verifyIdToken({
			idToken: token,
			audience: creds.MOBILE_CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
		});

		// get serialized info of the access token
		const payload = ticket.getPayload();

		// callback on payload
		return next(null, payload);
	} catch (err) {
		// callback on error
		return next(err);
	}
}

module.exports = {

	// set up routes and configure authentication settings
	init: function(app, passport) {

		passport.serializeUser(function(user, done) {
			// check for user in users table, that is NOT archived
			con.query('SELECT * FROM users WHERE email = ? AND isArchived = 0;', [user._json.email], function(err, rows) {
				if (!err && rows !== undefined && rows.length > 0) {
					// cache DB info in session
					user.local = rows[0];

					// if user has no name on record (perhaps user added by admin)
					if (user.local.name == null || user.local.firstName == null) {
						// update cached and DB name to reflect display name & first name
						user.local.name = user.displayName;
						user.local.firstName = user.name.givenName;

						// update user name in DB
						con.query('UPDATE users SET name = ?, firstName = ? WHERE uid = ?;', [user.displayName, user.name.givenName, user.local.uid], function(err) {
							done(null, user);
						});
					} else {
						// successfully pass user through
						done(null, user);
					}
				} else {
					// store in session that user attempted to authenticate and failed
					user.showAuthFailureMessage = true;

					// pass user through (they will NOT be considered authenticated as they do not have a user.local object)
					done(null, user);
				}
			});
		});

		passport.deserializeUser(function(user, done) {
			done(null, user);
		});

		// Google OAuth2 config with passport
		passport.use(new GoogleStrategy({
				clientID:		creds.GOOGLE_CLIENT_ID,
				clientSecret:	creds.GOOGLE_CLIENT_SECRET,
				callbackURL:	creds.domain + "/auth/google/callback",
				passReqToCallback: true,

				// tells passport to use userinfo endpoint instead of Google+
				userProfileURL: "https://www.googleapis.com/oauth2/v3/userinfo"
			},
			function(request, accessToken, refreshToken, profile, done) {
				process.nextTick(function () {
					return done(null, profile);
				});
			}
		));

		app.use(passport.initialize());
		app.use(passport.session());

		// authentication with google endpoint
		app.get('/auth/google', module.exports.checkReturnTo, passport.authenticate('google', { scope: [
				'profile',
				'email'
			],
			prompt : "select_account"	// ensure user always has to select their account
		}));

		// callback for google auth
		app.get('/auth/google/callback',
			passport.authenticate('google', {
				successReturnToOrRedirect: '/',
				failureRedirect: '/failure'
		}));

		// handler for failure to authenticate
		app.get('/failure', function(req, res) {
			res.render('error.html', { message: "Unable to authenticate." });
		});

		// logout handler
		app.get('/logout', module.exports.checkReturnTo, function(req, res) {
			req.logout();
			res.redirect(req.session.returnTo || '/');
		});

		// check if a mobile user is authenticated or not based on access ID
		app.post('/authMobile', function(req, res) {			
			// attempt to verify token through Google
			verify(req.body.accessId, function(err, profile) {
				if (!err && profile != null) {
					// check if user exists under this email
					con.query('SELECT * FROM users WHERE email = ? AND isArchived = 0;', [profile.email], function(err, rows) {
						if (!err && rows !== undefined && rows.length > 0) {
							// cache the latest access Id
							con.query('UPDATE users SET latestAccessId = ? WHERE uid = ?;', [req.body.accessId, rows[0].uid], function(err) {
								if (!err) {
									// respond affirmatively
									res.send({ isAuthenticated: 1 });
								} else {
									// respond with error
									res.send({ isAuthenticated: 0, error: "Error occurred caching latest access ID" });
								}
							});
						} else {
							// respond negatively
							res.send({ isAuthenticated: 0 });
						}
					});
				} else {
					// respond negatively
					res.send({ isAuthenticated: 0 });
				}
			});
		});

		return module.exports;
	},

	// middleware to check for a URL to return to after authenticating
	checkReturnTo: function(req, res, next) {
		var returnTo = req.query['returnTo'];
		if (returnTo) {
			req.session = req.session || {};
			req.session.returnTo = querystring.unescape(returnTo);
		}
		next();
	},

	// render the authentication failure page
	renderAuthFailure: function(req, res) {
		// extract user email from session
		var email = req.user._json ? req.user._json.email : null;

		// record that auth failure message has been displayed to user, so it will not be displayed a second time
		req.user.showAuthFailureMessage = false;

		// render auth failure page, so they can attempt another login
		res.render('authFailure.html', { 
			returnTo: querystring.escape(req.url), 
			email: email,
			auth: {
				message: "Welcome!"
			}
		});
	},

	// middleware to restrict pages (GET requests) to authenticated users
	isAuthGET: function(req, res, next) {
		// if authenticated
		if (req.isAuthenticated()) {
			// if system session data exists
			if (req.user && req.user.local) {
				return next();

			// if authentication failed
			} else if (req.user && req.user.showAuthFailureMessage) {
				// render auth failure page to notify them & allow to re-auth
				module.exports.renderAuthFailure(req, res);

			// if just not authenticated, sent them to auth screen
			} else {
				res.redirect('/auth/google?returnTo=' + querystring.escape(req.url));
			}
		} else {
			// send to auth screen to log in
			res.redirect('/auth/google?returnTo=' + querystring.escape(req.url));
		}
	},

	// middleware to restrict pages (GET requests) to admin users
	isAdminGET: function(req, res, next) {
		// if authenticated
		if (req.isAuthenticated()) {
			// if system session data exists and user is admin
			if (req.user && req.user.local) {
				// if admin, let request through
				if (req.user.local.isAdmin) {
					return next();
				} else {
					// render error page with link to /
					res.render('error.html', { 
						message: "You must be an administrator to access this page.",
						link: { href: '/', text: 'Return to homepage' }
					});
				}

			// if authentication failed
			} else if (req.user && req.user.showAuthFailureMessage) {
				// render auth failure page to notify them & allow to re-auth
				module.exports.renderAuthFailure(req, res);

			// if just not authenticated, sent them to auth screen
			} else {
				res.redirect('/auth/google?returnTo=' + querystring.escape(req.url));
			}
		} else {
			// send to auth screen to log in
			res.redirect('/auth/google?returnTo=' + querystring.escape(req.url));
		}
	},

	// middleware to check if POST request is authenticated
	isAuthPOST: function(req, res, next) {
		if (req.isAuthenticated() && req.user.local) {
			return next();
		} else {
			res.redirect('/');
		}
	},

	// middleware to check if POST request is from admin
	isAdminPOST: function(req, res, next) {
		if (req.isAuthenticated() && req.user.local && parseInt(req.user.local.isAdmin, 10) == 1) {
			return next();
		} else {
			res.redirect('/');
		}
	},

	// middleware to check if POST request to /sync is authenticated user
	isAuthSYNC: function(req, res, next) {
		// check for an authenticated user with these credentials
		con.query('SELECT * FROM users WHERE latestAccessId = ? AND email = ? AND isArchived = 0;', [req.body.accessId, req.body.email], function(err, rows) {
			if (!err && rows !== undefined && rows.length > 0) {
				return next();
			} else {
				// if unable to authenticate, respond and register error if any
				res.send({ message: "Unable to authenticate sync request.", error: err ? "An error occurred" : null });
			}
		});
	}
}