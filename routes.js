
var moment				= require('moment');						// include moment library
var con					= require('./database.js').connection;		// include database connection
var auth 				= require('./auth.js');						// include auth file
var sync 				= require('./sync.js');						// include sync file
var tools 				= require('./tools.js');					// include tools file
var student 			= require('./student.js'); 					// include student file
var attendanceReport 	= require('./attendanceReport.js');			// include reports file
var sys 				= require('./systemSettings.js');			// include settings file
var stats 				= require('./stats.js');					// include stats file

module.exports = {
	// set up all routes
	init: function(app) {

		// on post to /sync, sync with mobile device
		app.post('/sync', auth.isAuthSYNC, function(req, res) {
			// run a sync with the mobile device
			sync.handleSync(req, res);
		});

		// welcome page
		app.get('/welcome', function(req, res) {
			// render the welcome login screen with default render
			res.render('welcome.html', defaultRender(req));
		});

		// render home login screen
		app.get('/', function(req, res) {
			// if user is fully authenticated (Google OAuth & has system account)
			if (req.isAuthenticated() && req.user && req.user.local) {
				// start with default render object
				var render = defaultRender(req);

				// render homepage with system defaults for timeframe, class type
				renderHomePage(render, null, null, sys.defaultHomePageTimeframeAbbrev, res);
			} else {
				// if user has failed auth
				if (req.user && req.user.showAuthFailureMessage) {
					// show them auth failure page
					auth.renderAuthFailure(req, res);
				} else {
					// redirect to the welcome login screen (assuming first time auth)
					res.redirect('/welcome');
				}
			}
		});

		// post new parameters to /, render hompage with new data
		app.post('/', auth.isAuthPOST, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// default the class type if necessary
			if (!req.body.classTypeUID || req.body.classTypeUID == -1) {
				req.body.classTypeUID = null;
			}

			// default the classroom if necessary
			if (!req.body.classroomUID || req.body.classroomUID == -1) {
				req.body.classroomUID = null;
			}

			// default the timeframe if necessary
			if (!req.body.timeframe) {
				req.body.timeframe = sys.defaultHomePageTimeframeAbbrev;
			}

			// render the homepage
			renderHomePage(render, req.body.classTypeUID, req.body.classroomUID, req.body.timeframe, res);
		});

		// render search results page for empty query
		app.get('/searchStudents', auth.isAuthGET, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// render student search page using empty query
			renderStudentSearchPage(render, '', res);
		});

		// render search results for student search by name
		app.post('/searchStudents', auth.isAuthPOST, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// render search results using given query
			renderStudentSearchPage(render, req.body.query, res);
		});

		// render profile page for given student
		app.get('/students/:id', auth.isAuthGET, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// get start and end date of current month as timeframe for retrieving attendance records
			var attendanceStartDate = moment().startOf('month');
			var attendanceEndDate = moment().endOf('month');

			// render student page for this UID with default disciplinary history timeframe
			renderStudentPage(render, req.params.id, sys.defaultDiscHistoryTimeframe, attendanceStartDate, attendanceEndDate, res);
		});

		// render students page with new timeframe parameters
		app.post('/students/:id', auth.isAuthPOST, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// default disciplinary history timeframe if null
			if (!req.body.discHistoryTimeframe) {
				req.body.discHistoryTimeframe = sys.defaultDiscHistoryTimeframe;
			}

			var attendanceStartDate, attendanceEndDate;

			// if either date is undefined, default timeframe to past month
			if (!req.body.attendanceStartDate || !req.body.attendanceEndDate) {
				attendanceStartDate = moment().startOf('month');
				attendanceEndDate = moment().endOf('month');
			} else {
				// parse start and end dates for retrieving attendance records
				attendanceStartDate = moment(req.body.attendanceStartDate);
				attendanceEndDate = moment(req.body.attendanceEndDate);
			}

			// render student page for this UID with given disciplinary history timeframe
			renderStudentPage(render, req.params.id, req.body.discHistoryTimeframe, attendanceStartDate, attendanceEndDate, res);
		});

		// render page with report info
		app.get('/reports/:id', auth.isAuthGET, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// retrieve report information with this UID, if exists
			attendanceReport.getAttendanceReport(req.params.id, function(err, report) {
				if (!err) {
					// add all of report to render object
					render = Object.assign(render, report);

					// register whether or not records & teachers exist
					render.recordsExist = render.records.length > 0;
					render.teachersExist = render.teachers.length > 0;

					// retrieve whether or not the report can be edited (to display link to /editReport)
					attendanceReport.isEditable(req.params.id, function(err, isEditable) {
						// record whether or not editable (if error, assume uneditable)
						render.reportIsEditable = isEditable;

						// render report page with given info
						res.render('report.html', render);
					});
				} else {
					// render error page
					res.render('error.html', Object.assign(render, { 
						message: "The report you are looking for could not be found.",
						link: {
							href: "/searchReports",
							text: "Search for reports."
						},
						function: "attendanceReport.getAttendanceReport",
						raw: err
					}));
				}
			});
		});

		// render system tools page
		app.get('/tools', auth.isAuthGET, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// get necessary info to render tools page
			con.query('SELECT * FROM classrooms WHERE uid != 1; SELECT * FROM classTypes WHERE uid != 1; SELECT * FROM snacks WHERE uid != 1; SELECT * FROM users WHERE uid != ?;', [req.user.local.uid], function(err, rows) {
				if (!err && rows !== undefined && rows.length > 3) {
					// add info to render object
					render = Object.assign(render, {
						classrooms: rows[0],
						classTypes: rows[1],
						snacks: rows[2],
						teachers: [],
						admins: []
					});

					// for each user
					for (var i = 0; i < rows[3].length; i++) {
						var user = rows[3][i];

						// add user to appropriate array based on isAdmin status
						if (user.isAdmin) {
							render.admins.push(user);
						} else {
							render.teachers.push(user);
						}
					}

					// register which things exist
					render.classroomsExist = render.classrooms.length > 0;
					render.classTypesExist = render.classTypes.length > 0;
					render.snacksExist = render.snacks.length > 0;
					render.adminsExist = render.admins.length > 0;
					render.teachersExist = render.teachers.length > 0;

					// render page with options filled in
					res.render('tools.html', render);
				} else {
					// render error page
					res.render('error.html', Object.assign(render, { 
						message: "The necessary info for using system tools (existing classrooms, class types, etc) could not be found.",
						raw: err
					}));
				}
			});
		});

		// add a new classroom to the DB, by name
		app.post('/newClassroom', auth.isAuthPOST, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// ensure given name exists
			if (req.body.classroomName) {
				// create new classroom entry in DB
				tools.createClassroom(req.body.classroomName, function(err) {
					if (!err) {
						// render success page
						res.render('success.html', Object.assign(render, { 
							message: "The classroom \"" + req.body.classroomName + "\" was successfully created!",
							link: {
								href: "/tools",
								text: "Return to System Tools"
							}
						}));
					} else {
						// render error page
						res.render('error.html', Object.assign(render, { 
							message: "The system was unable to create the classroom \"" + req.body.classroomName + "\".",
							link: {
								href: "/tools",
								text: "Return to System Tools"
							},
							function: "tools.createClassroom",
							raw: err
						}));
					}
				});
			} else {
				// render error page for lack of name
				res.render('error.html', Object.assign(render, {
					message: "The system was unable to create a new classroom as no name was given.",
					link: {
						href: "/tools",
						text: "Return to System Tools"
					}
				}));
			}
		});

		// remove a classroom from the DB, by UID
		app.post('/removeClassroom', auth.isAuthPOST, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// ensure given UID exists
			if (req.body.classroomUID) {
				// delete classroom entry from DB
				tools.deleteClassroom(req.body.classroomUID, function(err) {
					if (!err) {
						// render success page
						res.render('success.html', Object.assign(render, { 
							message: "The system successfully deleted the indicated classroom!",
							link: {
								href: "/tools",
								text: "Return to System Tools"
							}
						}));
					} else {
						// render error page
						res.render('error.html', Object.assign(render, {
							message: "The system was unable to remove the indicated classroom.",
							link: {
								href: "/tools",
								text: "Return to System Tools"
							},
							function: "tools.deleteClassroom",
							raw: err
						}));
					}
				});
			} else {
				// render error page for lack of UID
				res.render('error.html', Object.assign(render, {
					message: "The system could not remove the classroom as none was indicated.",
					link: {
						href: "/tools",
						text: "Return to System Tools"
					}
				}));
			}
		});

		// add a new class type to the DB, by name and isAdult boolean
		app.post('/newClassType', auth.isAuthPOST, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// ensure given name and isAdult exist
			if (req.body.classTypeName && req.body.isAdult != null) {
				// create new classtype entry in DB
				tools.createClassType(req.body.classTypeName, req.body.isAdult, function(err) {
					if (!err) {
						// render success page
						res.render('success.html', Object.assign(render, { 
							message: "The new class type \"" + req.body.classTypeName + "\" was successfully created!",
							link: {
								href: "/tools",
								text: "Return to System Tools"
							}
						}));
					} else {
						// render error page
						res.render('error.html', Object.assign(render, {
							message: "The system was unable to create the new class type \"" + req.body.classTypeName + "\".",
							link: {
								href: "/tools",
								text: "Return to System Tools"
							},
							function: "tools.createClassType",
							raw: err
						}));
					}
				});
			} else {
				// render error page for lack of name
				res.render('error.html', Object.assign(render, {
					message: "The system could not create a new class type as no name was given.",
					link: {
						href: "/tools",
						text: "Return to System Tools"
					}
				}));
			}
		});

		// delete an existing class type from DB, by UID
		app.post('/removeClassType', auth.isAuthPOST, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// ensure given UID exists
			if (req.body.classTypeUID) {
				// delete classroom entry from DB
				tools.deleteClassType(req.body.classTypeUID, function(err) {
					if (!err) {
						// render success page
						res.render('success.html', Object.assign(render, { 
							message: "The system successfully deleted the indicated class type!",
							link: {
								href: "/tools",
								text: "Return to System Tools"
							}
						}));
					} else {
						// render error page
						res.render('error.html', Object.assign(render, {
							message: "The system was unable to remove the indicated class type.",
							link: {
								href: "/tools",
								text: "Return to System Tools"
							},
							function: "tools.deleteClassType",
							raw: err
						}));
					}
				});
			} else {
				// render error page for lack of UID
				res.render('error.html', Object.assign(render, { 
					message: "The system could not remove the class type as none was indicated.",
					link: {
						href: "/tools",
						text: "Return to System Tools"
					}
				}));
			}
		});

		// add a new snack type to DB, by name
		app.post('/newSnack', auth.isAuthPOST, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// ensure given name exists
			if (req.body.snackName) {
				// create new classtype entry in DB
				tools.createSnack(req.body.snackName, function(err) {
					if (!err) {
						// render success page
						res.render('success.html', Object.assign(render, { 
							message: "The system successfully created the new snack type \"" + req.body.snackName + "\"!",
							link: {
								href: "/tools",
								text: "Return to System Tools"
							}
						}));
					} else {
						// render error page
						res.render('error.html', Object.assign(render, {
							message: "The system was unable to create the new snack \"" + req.body.snackName + "\".",
							link: {
								href: "/tools",
								text: "Return to System Tools"
							},
							function: "tools.createSnack",
							raw: err
						}));
					}
				});
			} else {
				// render error page for lack of name
				res.render('error.html', Object.assign(render, {
					message: "The system could not create the new snack, as no name was given.",
					link: {
						href: "/tools",
						text: "Return to System Tools"
					}
				}));
			}
		});

		// delete a snack type from DB, by UID
		app.post('/removeSnack', auth.isAuthPOST, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// ensure given UID exists
			if (req.body.snackUID) {
				// delete snack entry from DB
				tools.deleteSnack(req.body.snackUID, function(err) {
					if (!err) {
						// render success page
						res.render('success.html', Object.assign(render, { 
							message: "The system successfully deleted the indicated snack!",
							link: {
								href: "/tools",
								text: "Return to System Tools"
							}
						}));
					} else {
						// render error page
						res.render('error.html', Object.assign(render, {
							message: "The system was unable to delete the snack indicated.",
							link: {
								href: "/tools",
								text: "Return to System Tools"
							},
							function: "tools.deleteSnack",
							raw: err
						}));
					}
				});
			} else {
				// render error page for lack of UID
				res.render('error.html', Object.assign(render, { 
					message: "The system could not delete the snack as none was indicated.",
					link: {
						href: "/tools",
						text: "Return to System Tools"
					}
				}));
			}
		});

		// add new user account to DB, by email & isAdmin status. Only available to admins.
		app.post('/newUser', auth.isAdminPOST, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// ensure given email, name and isAdmin exist
			if (req.body.email && req.body.name && req.body.isAdmin) {
				// create new user entry in DB
				tools.authorizeUser(req.body.email, req.body.name, req.body.isAdmin, function(err) {
					if (!err) {
						var message;

						// display success message based on admin status
						if (req.body.isAdmin == '1') {
							message = "Successfully authorized " + req.body.name + " with email " + req.body.email + " as an administrator.";
						} else {
							message = "Successfully authorized " + req.body.name + " with email " + req.body.email + " as a teacher.";
						}

						// render success page
						res.render('success.html', Object.assign(render, { 
							message: message,
							link: {
								href: "/tools",
								text: "Return to System Tools"
							}
						}));
					} else {
						// render error page
						res.render('error.html', Object.assign(render, { 
							message: "The system was unable to authorize the email \"" + req.body.email + "\" as " + (req.body.isAdmin == '1' ? "an administrator." : "a teacher."),
							link: {
								href: "/tools",
								text: "Return to System Tools"
							},
							function: "tools.authorizeUser",
							raw: err
						}));
					}
				});
			} else {
				// render error page for lack of name
				res.render('error.html', Object.assign(render, { 
					message: "The system was unable to authorize a new user account, as not all of the required fields were supplied (Email, name, and admin status).",
					link: {
						href: "/tools",
						text: "Return to System Tools"
					}
				}));
			}
		});

		// remove a user account from the DB, by UID. Prevent request from removing themself. Only available to admins.
		app.post('/removeUser', auth.isAdminPOST, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// ensure given UID exists and is not the UID of the user in the request
			if (req.body.userUID && parseInt(req.body.userUID, 10) != req.user.local.uid) {
				// delete user entry from DB
				tools.deauthorizeUser(req.body.userUID, function(err) {
					if (!err) {
						// render success page
						res.render('success.html', Object.assign(render, { 
							message: "The system successfully deauthorized the indicated user account!",
							link: {
								href: "/tools",
								text: "Return to System Tools"
							}
						}));
					} else {
						// render error page
						res.render('error.html', Object.assign(render, { 
							message: "The system was unable to remove the indicated user.",
							link: {
								href: "/tools",
								text: "Return to System Tools"
							},
							function: "tools.deauthorizeUser",
							raw: err
						}));
					}
				});
			} else {
				// render error page for lack of UID
				res.render('error.html', Object.assign(render, { 
					message: "The system could not remove the user, as none was indicated.",
					link: {
						href: "/tools",
						text: "Return to System Tools"
					}
				}));
			}
		});

		// do what when an admin updates the archival status of a user
		app.post('/updateArchive', auth.isAdminPOST, function(req, res) {
			var render = defaultRender(req);
			var status = parseInt(req.body.isArchived, 10);

			// validate request
			if (req.body.userUID && (status == 0 || status == 1)) {
				tools.updateArchiveStatus(req.body.userUID, status, function(err) {
					if (!err) {
						// render success page
						res.render('success.html', Object.assign(render, { 
							message: "The system successfully updated the archive status of the indicated user account!",
							link: {
								href: "/tools",
								text: "Return to System Tools"
							}
						}));
					} else {
						// render error page
						res.render('error.html', Object.assign(render, { 
							message: "The system was unable to update the archive status of the indicated user.",
							link: {
								href: "/tools",
								text: "Return to System Tools"
							},
							function: "tools.updateArchiveStatus",
							raw: err
						}));
					}
				});
			} else {
				// render error page for lack of UID
				res.render('error.html', Object.assign(render, { 
					message: "The system could not update the archive status, as not all necessary fields were supplied.",
					link: {
						href: "/tools",
						text: "Return to System Tools"
					}
				}));
			}
		});

		// get edit page for a given student
		app.get('/editStudent/:id', auth.isAuthGET, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// retrieve student profile information by UID
			student.getStudentProfile(req.params.id, function(err, profile) {
				if (!err) {
					// add profile to render object
					render = Object.assign(render, profile);

					// render edit page with profile
					res.render('editStudent.html', render);
				} else {
					// error page
					res.render('error.html', Object.assign(render, { 
						message: "The student profile you are attempting to edit could not be found.",
						link: {
							href: "/searchStudents",
							text: "Search for student profiles"
						},
						function: "student.getStudentProfile",
						raw: err
					}));
				}
			});
		});

		// field request to update student profile
		app.post('/editStudent/:id', auth.isAuthPOST, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// add student UID to updated profile
			req.body.uid = req.params.id;

			// apply edits by overriding existing student with updated profile
			student.editStudent(req.body, function(err) {
				if (!err) {
					// redirect to student profile page
					res.redirect('/students/' + req.params.id);
				} else {
					// error page
					res.render('error.html', Object.assign(render, {
						message: "The system was unable to update this student's profile.",
						link: {
							href: "/editStudent/" + req.params.id,
							text: "Return to their edit page"
						},
						function: "student.editStudent",
						raw: err
					}));
				}
			});
		});

		// get new student page
		app.get('/newStudent', auth.isAuthGET, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// render new student page
			res.render('newStudent.html', render);
		});

		// post profile to create new student
		app.post('/newStudent', auth.isAuthPOST, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// create student with given profile information
			student.createStudent(req.body, function(err, studentUID) {
				if (!err) {
					// redirect to new student's profile page
					res.redirect('/students/' + studentUID);
				} else {
					// error page
					res.render('error.html', Object.assign(render, { 
						message: "The system was unable to create a new student profile with the information provided.",
						link: {
							href: "/newStudent",
							text: "Return to new student page"
						},
						function: "student.createStudent",
						raw: err
					}));
				}
			});
		});

		// render report search page with defaults
		app.get('/searchReports', auth.isAuthGET, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// default to search without any specific classroom or class type
			var classroomUID = null, classTypeUID = null;

			// default date range to cover the past month
			var startDate = moment().subtract(1, 'months');
			var endDate = moment();

			// render search results page with these default parameters
			renderReportSearchPage(render, startDate, endDate, classroomUID, classTypeUID, res);
		});

		// get new search parameters and render results page accordingly
		app.post('/searchReports', auth.isAuthPOST, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// parse dates from request into moment objects
			var startDate = moment(req.body.startDate);
			var endDate = moment(req.body.endDate);

			// convert from -1 convention to null for classroom parameter
			if (req.body.classroomUID == -1) {
				req.body.classroomUID = null;
			}

			// convert from -1 convention to null for class type parameter
			if (req.body.classTypeUID == -1) {
				req.body.classTypeUID = null;
			}

			// render search results page with these parameters
			renderReportSearchPage(render, startDate, endDate, req.body.classroomUID, req.body.classTypeUID, res);
		});

		// delete an attendance report from DB
		app.post('/deleteStudent/:id', auth.isAuthPOST, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// remove student from DB by UID
			student.deleteStudent(req.body.studentUID, function(err) {
				if (!err) {
					// render success page
					res.render('success.html', Object.assign(render, {
						alternateTitle: "Student Deleted",
						message: "The student was removed from the system.",
						link: {
							href: "/searchStudents",
							text: "Search for students"
						}
					}));
				} else {
					// render error page for failure to delete
					res.render('error.html', Object.assign(render, {
						message: "The system failed to delete the indicated student.",
						link: {
							href: "/editStudent/" + req.params.id,
							text: "Back to this student's edit page"
						}
					}));
				}
			});
		});

		// delete a student profile from DB
		app.post('/deleteReport/:id', auth.isAuthPOST, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// check if the report is within the editing timeframe
			attendanceReport.isEditable(req.body.reportUID, function(err, isEditable) {
				if (!err) {
					if (isEditable) {
						// remove report by UID
						attendanceReport.deleteAttendanceReport(req.body.reportUID, function(err) {
							if (!err) {
								// render success page on delete
								res.render('success.html', Object.assign(render, {
									message: "The system successfully deleted the indicated attendance report!",
									link: {
										href: "/searchReports/",
										text: "Search for reports"
									}
								}));
							} else {
								// render error for failure to delete
								res.render('error.html', Object.assign(render, {
									message: "The system was unable to delete the indicated attendance report.",
									link: {
										href: "/editReport/" + req.params.id,
										text: "Back to this report's edit page"
									},
									function: "attendanceReport.deleteAttendanceReport",
									raw: err
								}));
							}
						});
					} else {
						// render error for inability for outside editing timeframe
						res.render('error.html', Object.assign(render, {
							message: "You are unable to delete this attendance report as it is past its editing cutoff.",
							link: {
								href: "/reports/" + req.params.id,
								text: "Back to this report's page"
							}
						}));
					}
				} else {
					// render error for failure to check editing timeframe
					res.render('error.html', Object.assign(render, {
						message: "The system was unable determine the editing timeframe for this attendance report.",
						link: {
							href: "/reports/" + req.params.id,
							text: "Back to this report's page"
						},
						function: "attendanceReport.isEditable",
						raw: err
					}));
				}
			});
		});

		// get the page to add a new report
		app.get('/newReport', auth.isAuthGET, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// add maximum disciplinary points to render object for interface
			render.maxDiscPoints = sys.maxDiscPoints;

			// construct query to get all metadata for options
			let query = 'SELECT * FROM classrooms ORDER BY uid;'
			query += 'SELECT * FROM classTypes ORDER BY uid;';
			query += 'SELECT * FROM snacks ORDER BY uid;';
			query += 'SELECT * FROM users WHERE isArchived = 0;';

			// get necessary info from DB for
			con.query(query, function(err, rows) {
				if (!err && rows !== undefined && rows.length > 3) {
					// add info to render object
					render.classrooms = rows[0];
					render.classTypes = rows[1];
					render.snacks = rows[2];
					render.teachers = rows[3];

							// get all students in the database
					student.getAllStudents(function(err, students) {
						if (!err) {
							// store searchable students
							render.students = students;
						}

						// render edit page for this report
						res.render('newReport.html', render);
					});
				} else {
					// error page
					res.render('error.html', Object.assign(render, { 
						message: "The system was unable to retrieve data necessary for interacting with the report editing interface.",
						link: {
							href: "/reports/" + req.params.id,
							text: "Return to this report's page"
						},
						raw: err
					}));
				}
			});
		});

		// create a new attendance report
		app.post('/newReport', auth.isAuthPOST, function(req, res) {
			// get default render object
			var render = defaultRender(req);
			var report = req.body.report;

			// add new attendance report, get its inserted UID
			con.query('INSERT INTO attendanceReports (classroomUID, classTypeUID, snackUID, comments, whenRecorded) VALUES (?, ?, ?, ?, NOW()); SELECT LAST_INSERT_ID() AS reportUID;', [report.classroomUID, report.classTypeUID, report.snackUID, report.comments], function(err, rows) {
				if (!err && rows !== undefined && rows.length > 1 && rows[1].length > 0) {
					report.uid = rows[1][0].reportUID;

					// if no individual records exist--throw an error
					if (!report.records) {
						// remove the failed report
						attendanceReport.deleteAttendanceReport(report.uid, function(err) {
							res.send({ err: "Failed to add class report: There are no student records added to this report!" });
						});
					} else {
						var indivRecords = [];

						// make individual records into an insertable format
						for (var i = 0; i < report.records.length; i++) {
							indivRecords.push([report.uid, report.records[i].studentUID, report.records[i].didEat, report.records[i].disciplinePoints, report.records[i].comments]);
						}

						// insert the individual records
						con.query('INSERT INTO individualRecords (reportUID, studentUID, didEat, disciplinePoints, comments) VALUES ?;', [indivRecords], function(err) {
							if (!err) {
								// if no teachers were linked--throw an error
								if (!report.teachers) {
									// remove the failed report
									attendanceReport.deleteAttendanceReport(report.uid, function(err) {
										res.send({ err: "Failed to add class report: There are no teachers associated with this report!" });
									});
								} else {
									var teacherLinks = [];

									// make teachers linked to this report insertable
									for (var i = 0; i < report.teachers.length; i++) {
										teacherLinks.push([report.uid, report.teachers[i]]);
									}

									// insert the teacher report links
									con.query('INSERT INTO teacherReportLink (reportUID, teacherUID) VALUES ?;', [teacherLinks], function(err) {
										if (!err) {
											// redirect to this report's page
											res.send({ uid: report.uid });
										} else {
											// remove the failed report
											attendanceReport.deleteAttendanceReport(report.uid, function(err) {
												res.send({ err: "Failed to add teachers to this report." });
											});
										}
									});
								}
							} else {
								// remove the failed report
								attendanceReport.deleteAttendanceReport(report.uid, function(err) {
									res.send({ err: "Failed to add individual student records of this report."})
								});
							}
						});
					}
				} else {
					res.send({ err: "Failed to add class report." });
				}
			});
		});

		// add a new student, from the new report interface
		app.post('/newReport/newStudent', auth.isAuthPOST, function(req, res) {
			// create student with given profile information
			student.createStudent(req.body, function(err, studentUID) {
				res.send({ err, studentUID });
			});
		});

		// get the edit page for a given report, if within editing timeframe
		app.get('/editReport/:id', auth.isAuthGET, function(req, res) {
			// start with default render object
			var render = defaultRender(req);

			// add maximum disciplinary points to render object for interface
			render.maxDiscPoints = sys.maxDiscPoints;

			// check if report is editable
			attendanceReport.isEditable(req.params.id, function(err, isEditable) {
				if (!err) {
					// if report is currently editable
					if (isEditable) {
						// get attendance report's info
						attendanceReport.getAttendanceReport(req.params.id, function(err, report) {
							if (!err) {
								// add report to render object
								render.report = report;

								// construct query to get all metadata for options
								var query = 'SELECT * FROM classrooms ORDER BY uid;'
								query += 'SELECT * FROM classTypes ORDER BY uid;';
								query += 'SELECT * FROM snacks ORDER BY uid;';
								query += 'SELECT users.*, links.uid IS NOT NULL AS linked FROM users LEFT JOIN (SELECT * FROM teacherReportLink WHERE reportUID = ?) AS links ON users.uid = links.teacherUID WHERE users.isArchived = 0;';

								// get necessary info from DB for making changes to classroom, classtype, etc
								con.query(query, [req.params.id], function(err, rows) {
									if (!err && rows !== undefined && rows.length > 3) {
										// add info to render object
										render.classrooms = rows[0];
										render.classTypes = rows[1];
										render.snacks = rows[2];
										render.teachers = rows[3];

										// select this report's current classroom
										for (var i = 0; i < render.classrooms.length; i++) {
											// if same as currently selected classroom
											if (render.classrooms[i].uid == render.report.classroomUID) {
												render.classrooms[i].selected = true;
												break;
											}
										}

										// select this report's current classroom
										for (var i = 0; i < render.classTypes.length; i++) {
											// if same as currently selected classroom
											if (render.classTypes[i].uid == render.report.classTypeUID) {
												render.classTypes[i].selected = true;
												break;
											}
										}

										// select this report's current classroom
										for (var i = 0; i < render.snacks.length; i++) {
											// if same as currently selected classroom
											if (render.snacks[i].uid == render.report.snackUID) {
												render.snacks[i].selected = true;
												break;
											}
										}

										// render edit page for this report
										res.render('editReport.html', render);
									} else {
										// error page
										res.render('error.html', Object.assign(render, { 
											message: "The system was unable to retrieve data necessary for interacting with the report editing interface.",
											link: {
												href: "/reports/" + req.params.id,
												text: "Return to this report's page"
											},
											raw: err
										}));
									}
								});
							} else {
								// render error message for failing to get attendance report info
								res.render('error.html', Object.assign(render, {
									message: "The system was unable to retrieve this report's data for editing.",
									link: {
										href: "/reports/" + req.params.id,
										text: "Return to this report's page"
									},
									function: "attendanceReport.getAttendanceReport",
									raw: err
								}));
							}
						});
					} else {
						// render error message for being past editing cutoff
						res.render('error.html', Object.assign(render, {
							message: "You are unable to edit this report, as it is past its editing cutoff.",
							link: {
								href: "/reports/" + req.params.id,
								text: "Return to this report's page"
							},
							function: "attendanceReport.isEditable",
							raw: err
						}));
					}
				} else {
					// render error message
					res.render('error.html', Object.assign(render, {
						message: "The system was unable to determine whether the editing timeframe for this report.",
						link: {
							href: "/reports/" + req.params.id,
							text: "Return to this report's page"
						},
						function: "attendanceReport.isEditable",
						raw: err
					}));
				}
			});
		});

		// update an attendance report if within editing timeframe
		app.post('/editReport/:id', auth.isAuthPOST, function(req, res) {
			// get default render object
			var render = defaultRender(req);

			// correct for null teachers array, make empty array
			if (!req.body.report.teachers) {
				req.body.report.teachers = [];
			}

			// check if report is editable before applying updates
			attendanceReport.isEditable(req.params.id, function(err, isEditable) {
				if (!err) {
					// if report can be edited at this time
					if (isEditable) {
						// get report from request
						var report = req.body.report;

						// reset empty comments to null for DB
						if (report.comments == "") {
							report.comments = null;
						}

						// apply classroom, class type, snack, and comments updates to report
						con.query('UPDATE attendanceReports SET classroomUID = ?, classTypeUID = ?, snackUID = ?, comments = ? WHERE uid = ?;', [report.classroomUID, report.classTypeUID, report.snackUID, report.comments, req.params.id], function(err) {
							if (!err) {
								// array for update query to individual records table
								var indivRecordsUpdate = [];
								var updateIndivRecordsQuery = "";

								// add each individual record's values to array for query
								for (var i = 0; i < report.records.length; i++) {
									// reset empty comments to null
									if (report.records[i].comments == "") {
										report.records[i].comments = null;
									}

									// add variables to array for updating
									indivRecordsUpdate.push(report.records[i].didEat, report.records[i].disciplinePoints, report.records[i].comments, report.records[i].uid);
									updateIndivRecordsQuery += "UPDATE individualRecords SET didEat = ?, disciplinePoints = ?, comments = ? WHERE uid = ?;";
								}

								// update individual records associated with this report
								con.query(updateIndivRecordsQuery, indivRecordsUpdate, function(err) {
									if (!err) {
										// update the teachers who are linked to this report
										attendanceReport.updateTeachers(req.params.id, report.teachers, function(err) {
											if (!err) {
												// render success page for applying edits
												res.render('success.html', Object.assign(render, {
													message: "The system successfully updated the indicated report!",
													link: {
														href: "/reports/" + req.params.id,
														text: "Return to this report's page"
													},
													raw: err
												}));
											} else {
												// render error message
												res.render('error.html', Object.assign(render, {
													message: "The system failed to update the teachers associated with this report.",
													link: {
														href: "/reports/" + req.params.id,
														text: "Return to this report's page"
													},
													raw: err
												}));
											}
										});
									} else {
										// render error message
										res.render('error.html', Object.assign(render, {
											message: "The system failed to apply the updates records associated with the indicated report.",
											link: {
												href: "/reports/" + req.params.id,
												text: "Return to this report's page"
											},
											raw: err
										}));
									}
								});
							} else {
								// render error message
								res.render('error.html', Object.assign(render, {
									message: "The system failed to apply the updates to the indicated attendance report.",
									link: {
										href: "/reports/" + req.params.id,
										text: "Return to this report's page"
									},
									raw: err
								}));
							}
						});
					} else {
						// render error message for being past editing cutoff
						res.render('error.html', Object.assign(render, {
							message: "You are unable to edit this report, as it is past its editing cutoff.",
							link: {
								href: "/reports/" + req.params.id,
								text: "Return to this report's page"
							},
							raw: err
						}));
					}
				} else {
					// render error message for being past editing cutoff
					res.render('error.html', Object.assign(render, {
						message: "The system was unable to determine the editing cutoff for this report.",
						link: {
							href: "/reports/" + req.params.id,
							text: "Return to this report's page"
						},
						function: "attendanceReport.isEditable",
						raw: err
					}));
				}
			});
		});

		return module.exports;
	}
}

// generate a starter render object with basic required fields for navbar on all pages
function defaultRender(req) {
	if (req.isAuthenticated() && req.user && req.user.local) {
		// basic render object for fully authenticated user
		return {
			inDevMode: sys.inDevMode,
			auth: {
				isAuthenticated: true,
				userIsAdmin: req.user.local.isAdmin,
				message: "Welcome, " + req.user.name.givenName + "! (" + req.user.local.email + ")"
			}
		};
	} else {
		// default welcome message for unauthenticated user
		return {
			inDevMode: sys.inDevMode,
			auth: {
				message: "Welcome!"
			}
		};
	}
}

// copy system timeframes into a render object, and store full name of current timeframe
function copySysTimeframes(render, currentTimeframe) {
	render.timeframes = [];

	// copy timeframes array from system settings
	for (var abbrev in sys.timeframes) {
		if (sys.timeframes.hasOwnProperty(abbrev)) {
			var timeframe = sys.timeframes[abbrev];

			// copy name and abbreviation
			var copy = {
				abbrev: timeframe.abbrev,
				name: timeframe.name
			};

			// add selected=true attribute if timeframe matches given parameter
			if (copy.abbrev == currentTimeframe) {
				copy.selected = true;

				// add current timeframe full name to render object as well
				render.currentTimeframe = timeframe.name;
			}

			// add copy to render timeframes array
			render.timeframes.push(copy);
		}
	}
}

/*	get a moment object of a date based on given timeframe
	ie 2 Weeks would result in the date, two weeks ago */
function getDateFromTimeframe(abbrev) {
	// get current time
	var now = moment();

	// get system timeframe from abbreviation
	var tf = sys.timeframes[abbrev];

	// if timeframe exists and is not "All Time"
	if (tf && tf.abbrev != 'A') {
		// move backwards given duration
		return now.subtract(tf.duration);
	} else {
		// use start of century as "All Time"
		return moment('1900-01-01');
	}
}




/*
	------- Generic Functions for Rendering Each Parameterized Page -------
*/

// render the homepage with given parameters
function renderHomePage(render, classTypeUID, classroomUID, timeframe, res) {
	// start off object with basic system variables
	var render = Object.assign(render, {
		numAbsentStudentsDisplayed: sys.numAbsentStudentsDisplayed,
		numDaysAbsent: sys.numDaysAbsent,
		numDisciplinaryIssuesDisplayed: sys.numDisciplinaryIssuesDisplayed,
		numSnacks: sys.numSnacks,
		timeframeIsAllTime: timeframe == 'A'	// determine whether or not timeframe is all time (changes caption slightly)
	});

	// copy system timeframes into render object as options for selecting a timeframe
	copySysTimeframes(render, timeframe);


	// select all class types
	con.query('SELECT * FROM classTypes WHERE uid != 1; SELECT * FROM classrooms WHERE uid != 1;', function(err, rows) {
		if (!err && rows !== undefined && rows.length > 1) {
			var types = rows[0];
			// select given class type based on UID
			for (var i = 0; i < types.length; i++) {
				if (types[i].uid == classTypeUID) {
					types[i].selected = true;
					break;
				}
			}

			// add class types to object
			render.classTypes = types;

			var rooms = rows[1];
			// select given classroom based on UID
			for (var i = 0; i < rooms.length; i++) {
				if (rooms[i].uid == classroomUID) {
					rooms[i].selected = true;
					break;
				}
			}

			// add class types to object
			render.classrooms = rooms;

			// calculate date cutoff for queries based on timeframe
			var dateCutoff = getDateFromTimeframe(timeframe);

			// get actual statistics
			stats.getAllHomePageStats(dateCutoff, classroomUID, classTypeUID, function(err, obj) {
				if (!err) {
					// add content of homepage stats to render object
					render = Object.assign(render, obj);

					// get all students in the database
					student.getSearchableStudents(function(err, students) {
						if (!err) {
							// store searchable students
							render.searchableStudents = students;
						}

						// render homepage
						res.render('homepage.html', render);
					});
				} else {
					// show error page
					res.render('error.html', Object.assign(render, { 
						message: "The system was unable to retrieve the statistics needed for the homepage.",
						function: "stats.getAllHomePageStats",
						raw: err
					}));
				}
			});
		} else {
			// callback on error finding class types
			res.render('error.html', Object.assign(render, { 
				message: "The system was unable to retrieve class type data necessary for the homepage statistics.",
				raw: err
			}));
		}
	});
}

// render student page with given parameters
function renderStudentPage(render, studentUID, discTimeframe, attendanceStartDate, attendanceEndDate, res) {
	// add selected attendance data timeframe to render object
	render.attendanceStartDate = attendanceStartDate.format('YYYY-MM-DD');
	render.attendanceEndDate = attendanceEndDate.format('YYYY-MM-DD');

	// determine whether or not timeframe is all time (changes caption slightly)
	render.timeframeIsAllTime = discTimeframe == 'A';

	// copy system timeframes for use in disciplinary history chart
	copySysTimeframes(render, discTimeframe);

	// get student profile with this UID
	student.getStudentProfile(studentUID, function(err, profile) {
		if (!err) {
			// add all of profile to render object
			render = Object.assign(render, profile);

			// get this student's parent information, formatted into a single string (protect against issues with incomplete parent info)
			render.parentString = student.getParentString(profile);

			// get usual classroom and classtype of student
			student.getUsualClassData(studentUID, function(err, classroom, classType, frequency) {
				if (!err) {
					// record retrieved info in render object
					render.usualClassroom = classroom;
					render.usualClassType = classType;
					render.usualClassFrequency = frequency ? frequency.toFixed(1) : null;

					// get the cutoff date for default disciplinary history timeframe
					var cutoff = getDateFromTimeframe(discTimeframe);

					// get disciplinary data in timeframe
					student.getDisciplinaryDataForStudent(cutoff, studentUID, function(err, data) {
						if (!err) {
							// add disciplinary data to render object, register its existence
							render.disciplineData = stats.condenseChartData(data);
							render.disciplineDataExists = data.length > 0;

							// if chart data being averaged, register in render object
							if (data.length > sys.maxNumChartPoints) {
								render.disciplineDataAveraged = true;
							}

							// get individual records over past month (default timeframe)
							student.getRecordsByStudent(attendanceStartDate, attendanceEndDate, studentUID, function(err, records) {
								if (!err) {
									// add records to render object
									render.attendanceRecords = records;
									render.attendanceRecordsExist = records.length > 0;

									// render page
									res.render('students.html', render);
								} else {
									// render error retrieving records
									res.render('error.html', Object.assign(render, { 
										message: "The system was unable to retrieve the attendance records for this student. Please ensure the selected date range is valid.",
										link: {
											href: "/students/" + studentUID,
											text: "Back to student's page"
										},
										function: "student.getRecordsByStudent",
										raw: err
									}));
								}
							});
						} else {
							// render error page for getting disciplinary data
							res.render('error.html', Object.assign(render, { 
								message: "The system was unable to retrieve the disciplinary history of this student.",
								function: "student.getDisciplinaryDataForStudent",
								raw: err
							}));
						}
					});
				} else {
					// render error page for getting usual data
					res.render('error.html', Object.assign(render, { 
						message: "The system was unable to calculate this student's usual class information.",
						function: "student.getUsualClassData",
						raw: err
					}));
				}
			});
		} else {
			// render error for inability to find student profile
			res.render('error.html', Object.assign(render, { 
				message: "The student you are looking for could not be found.",
				link: {
					href: "/searchStudents",
					text: "Search for students"
				},
				function: "student.getStudentProfile",
				raw: err
			}));
		}
	});
}

// render student search results page with given query
function renderStudentSearchPage(render, query, res) {
	// search for student profiles close to given query
	student.searchStudents(query, sys.numStudentSearchResults, function(err, students) {
		if (!err) {
			// add data to render object
			render.resultsExist = students.length > 0;
			render.students = students;

			// add query to render object
			render.query = query;

			// get all students in the database
			student.getSearchableStudents(function(err, students) {
				if (!err) {
					// store searchable students
					render.searchableStudents = students;
				}

				// render student search results page
				res.render('searchStudents.html', render);
			});
		} else {
			// render error page
			res.render('error.html', Object.assign(render, { 
				message: "The system was unable to search for student profiles with the given query.",
				link: {
					href: "/searchStudents",
					text: "Try another search"
				},
				function: "student.searchStudents",
				raw: err
			}));
		}
	});
}

// render the report search results page with given parameters
function renderReportSearchPage(render, startDate, endDate, classroomUID, classTypeUID, res) {
	// format start and end dates into render object to maintain date parameters
	render.startDate = startDate.format('YYYY-MM-DD');
	render.endDate = endDate.format('YYYY-MM-DD');

	// get class data for search
	con.query('SELECT * FROM classrooms ORDER BY uid; SELECT * FROM classTypes ORDER BY uid;', function(err, rows) {
		if (!err && rows !== undefined && rows.length > 0) {
			// add class data to render object as search parameters
			render.classrooms = rows[0];
			render.classTypes = rows[1];

			// mark current classroom search parameter with selected=true
			for (var i = 0; i < render.classrooms.length; i++) {
				if (render.classrooms[i].uid == classroomUID) {
					render.classrooms[i].selected = true;
					break;
				}
			}

			// mark current class type search parameter with selected=true
			for (var i = 0; i < render.classTypes.length; i++) {
				if (render.classTypes[i].uid == classTypeUID) {
					render.classTypes[i].selected = true;
					break;
				}
			}

			// search attendance reports with given search parameters
			attendanceReport.searchAttendanceReports(startDate, endDate, classroomUID, classTypeUID, sys.numReportSearchResults, function(err, reports) {
				if (!err) {
					// store reports in render object, check if they exist
					render.reports = reports;
					render.reportsExist = reports.length > 0;

					// render search results page
					res.render('searchReports.html', render);
				} else {
					// render error page
					res.render('error.html', Object.assign(render, { 
						message: "The system was unable to search attendance reports with the given parameters.",
						link: {
							href: "/searchReports",
							text: "Try another search"
						},
						function: "attendanceReport.searchAttendanceReports",
						raw: err
					}));
				}
			});
		} else {
			// render error page
			res.render('error.html', Object.assign(render, { 
				message: "The system was unable to retrieve class data necessary for interacting with the attendance report search interface.",
				raw: err
			}));
		}
	});
}