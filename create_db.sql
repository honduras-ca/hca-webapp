
DROP DATABASE IF EXISTS HCA_attendance;
CREATE DATABASE HCA_attendance;

USE HCA_attendance;

-- classroom location names
CREATE TABLE classrooms (
	uid INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(32) UNIQUE,
	PRIMARY KEY (uid)
);

-- secondary differentiator for classes (i.e. "AM (Child)", "Introduccion (Adult)", ...)
CREATE TABLE classTypes (
	uid INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(32) UNIQUE,
	isAdult TINYINT(1),
	PRIMARY KEY (uid)
);

-- table of known snack types
CREATE TABLE snacks (
	uid INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(64) UNIQUE,
	PRIMARY KEY (uid)
);

-- all HCA student info
CREATE TABLE students (
	uid INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(150) NOT NULL,
	isAdult TINYINT(1) NOT NULL DEFAULT 0,
	parent1 VARCHAR(150),
	parent2 VARCHAR(150),
	address VARCHAR(150),
	phone VARCHAR(16),
	specialCircumstances TEXT,
	uniformSize VARCHAR(16),
	PRIMARY KEY (uid)
) ENGINE=InnoDB;

-- create index on students' names for search engine
CREATE FULLTEXT INDEX studentIndex ON students(name);

-- all attendance reports recorded on-site by teachers
CREATE TABLE attendanceReports (
	uid INT NOT NULL AUTO_INCREMENT,
	classroomUID INT NOT NULL,
	classTypeUID INT NOT NULL,
	snackUID INT,		-- snack UID can be null because adult classes have no snack (not 'unknown')
	whenRecorded DATETIME,
	comments TEXT,
	FOREIGN KEY (classroomUID) REFERENCES classrooms(uid),
	FOREIGN KEY (classTypeUID) REFERENCES classTypes(uid),
	FOREIGN KEY (snackUID) REFERENCES snacks(uid),
	PRIMARY KEY (uid)
) ENGINE=InnoDB;

-- all individual records that make up attendance reports
CREATE TABLE individualRecords (
	uid INT NOT NULL AUTO_INCREMENT,
	reportUID INT NOT NULL,
	studentUID INT NOT NULL,
	didEat TINYINT(1) DEFAULT 1,
	disciplinePoints INT DEFAULT 0,
	comments TEXT,
	FOREIGN KEY (studentUID) REFERENCES students(uid) ON DELETE CASCADE,
	FOREIGN KEY (reportUID) REFERENCES attendanceReports(uid) ON DELETE CASCADE,
	PRIMARY KEY (uid)
);

-- user profile information (teachers / admins)
CREATE TABLE users (
	uid INT NOT NULL AUTO_INCREMENT,
	email VARCHAR(32) UNIQUE,
	name VARCHAR(64),
	firstName VARCHAR(32),
	isAdmin TINYINT(1) DEFAULT 0,
	isArchived TINYINT(1) DEFAULT 0,		-- can the user still access the system and be linked to class reports
	latestAccessId VARCHAR(1500),
	PRIMARY KEY (uid)
);

-- association between attendance reports and the teachers that taught that class
CREATE TABLE teacherReportLink (
	uid INT NOT NULL AUTO_INCREMENT,
	reportUID INT NOT NULL,
	teacherUID INT NOT NULL,
	FOREIGN KEY (reportUID) REFERENCES attendanceReports(uid) ON DELETE CASCADE,
	FOREIGN KEY (teacherUID) REFERENCES users(uid) ON DELETE CASCADE,
	PRIMARY KEY (uid)
);

-- include "Unknown" default values for each table in which fields may be deleted
INSERT INTO classrooms (uid, name) VALUES (1, "Unknown");
INSERT INTO classTypes (uid, name) VALUES (1, "Unknown");
INSERT INTO snacks (uid, name) VALUES (1, "Unknown");

-- add default classroom data
INSERT INTO classrooms (name) VALUES 
	("PEP Centro"),
	("PEP-2"),
	("PEP-3"),
	("Volunteer House 1");

-- add default class type data
INSERT INTO classTypes (name, isAdult) VALUES 
	("AM (Child)", 0),
	("PM (Child)", 0),
	("Introducción (Adult)", 1),
	("Principiante (Adult)", 1),
	("Intermedio (Adult)", 1);